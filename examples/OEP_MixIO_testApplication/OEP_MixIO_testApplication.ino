/*
Name
  OEP_testApplication

Description
  Open Effect Pedal library test application

 */
#include <EEPROM.h>
#include <Bounce.h>
#include <Audio.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <RBD_SerialManager.h>
#include <OpenEffectPedal.h>
#include <OpenEffectPedal_APP_Helpers.h>

#define MAX_LED_VAL			127

#define STORAGE_OFFSET_OEP_MIXIO   0
#define STORAGE_OFFSET_OEP_CODEC   (STORAGE_OFFSET_OEP_MIXIO + OEP_APP_MIXIO_STORAGE_SIZE_IN_BYTES)
#define STORAGE_OFFSET_OEP_APPLI   (STORAGE_OFFSET_OEP_CODEC + 128)


AudioInputUSB             usb_in;
AudioOutputUSB            usb_out;

AudioInputI2S           	i2s_in;
AudioOutputI2S          	i2s_out;

AudioMixer4             	mixer1;
AudioMixer4               mixer2;

Mixer_Stereo_2to1         Mix_2to1;
Mixer_Stereo_1to2         Mix_1to2;

// Simple Passtrhu: I2S In -> Mixer -> I2S out
/*AudioConnection         	link01(i2s_in, 0, mixer1, 0);
AudioConnection         	link02(i2s_in, 1, mixer1, 1);
AudioConnection           link11(i2s_in, 0, mixer2, 0);
AudioConnection           link12(i2s_in, 1, mixer2, 1);
AudioConnection         	link03(mixer1, 0, i2s_out, 0);
AudioConnection         	link04(mixer2, 0, i2s_out, 1);*/
/*AudioConnection           lpbck01(i2s_in, 0, usb_out, 0);
AudioConnection           lpbck02(i2s_in, 1, usb_out, 1);
AudioConnection           lpbck11(usb_in, 0, i2s_out, 0);
AudioConnection           lpbck12(usb_in, 1, i2s_out, 1);*/
/*AudioConnection           lpbck01(i2s_in, 0, usb_out, 0);
AudioConnection           lpbck02(i2s_in, 1, usb_out, 1);
AudioConnection           lpbck11(usb_in, 0, mixer1, 0);
AudioConnection           lpbck12(usb_in, 1, mixer1, 1);
AudioConnection           lpbck21(usb_in, 0, mixer2, 0);
AudioConnection           lpbck22(usb_in, 1, mixer2, 1);
xAudioConnection           lpbck31(mixer1, 0, i2s_out, 0);
AudioConnection           lpbck32(mixer2, 0, i2s_out, 1);*/

AudioConnection           link01(i2s_in, 0, Mix_2to1, 0);
AudioConnection           link02(i2s_in, 1, Mix_2to1, 1);
AudioConnection           link11(usb_in, 0, Mix_2to1, 2);
AudioConnection           link12(usb_in, 1, Mix_2to1, 3);
AudioConnection           link21(Mix_2to1, 0, Mix_1to2, 0);
AudioConnection           link22(Mix_2to1, 1, Mix_1to2, 1);
AudioConnection           link31(Mix_1to2, 0, i2s_out, 0);
AudioConnection           link32(Mix_1to2, 1, i2s_out, 1);
AudioConnection           link41(Mix_1to2, 2, usb_out, 0);
AudioConnection           link42(Mix_1to2, 3, usb_out, 1);

RBD::SerialManager 			serial_manager;

OEP_Abstraction         OEP(E_MB_Type_Rev2_1, E_EXT_Type_1SI_2EX_1SO);
OEP_APP_MixIO           OEP_MixIO(&OEP, &Mix_2to1, &Mix_1to2, E_BUTTON_RIGHT, OEP_APP_MixIO::E_LINK_MODE_P1_P4, OEP_APP_MixIO::E_UI_MODE_VOL_SRC, OEP_APP_MixIO::E_SAVE_MODE_ENABLED, STORAGE_OFFSET_OEP_MIXIO);

int 	led_up     = MAX_LED_VAL;
int 	led_left   = MAX_LED_VAL;
int 	led_right  = MAX_LED_VAL;
int   led_others = MAX_LED_VAL;


float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


void setup()
{
  // Configure Serial
  serial_manager.start();
  delay(2000);

  OEP.DisableExpr1();
  OEP.DisableExpr2();
  int iRet = OEP.setup();
  if (iRet != 0)
  {
    serial_manager.println("OEP setup failed: ");
    serial_manager.print("  Input abstraction = ");
    serial_manager.println(OEP.iSetupRet_InputAbstraction);
    serial_manager.print("  Output abstraction = ");
    serial_manager.println(OEP.iSetupRet_OutputAbstraction);
    serial_manager.print("  Audio abstraction = ");
    serial_manager.println(OEP.iSetupRet_AudioAbstraction);
  }
  OEP.SetThresholdPot(5);

  (void)OEP.Register_Button_Callback(E_BUTTON_LEFT, E_BUTTON_LONG_PUSH_PRESSING, fCallback_LeftButton_LongPush_Press, NULL);
  (void)OEP.Register_Button_Callback(E_BUTTON_LEFT, E_BUTTON_LONG_PUSH_RELEASE, fCallback_LeftButton_LongPush_Release, NULL);
  (void)OEP.Register_Switch_Pot_Callback(E_POT_1, E_SWITCH_LEFT, E_SWITCH_STATE_LEFT, fCallback_Pot1_SwitchL_StateLeft, (void*)1);
  (void)OEP.Register_Switch_Pot_Callback(E_POT_2, E_SWITCH_LEFT, E_SWITCH_STATE_MIDDLE, fCallback_Pot2_SwitchL_StateMiddle, (void*)2);
  (void)OEP.Register_Switch_Pot_Callback(E_POT_3, E_SWITCH_LEFT, E_SWITCH_STATE_RIGHT, fCallback_Pot3_SwitchL_StateRight, (void*)3);
  (void)OEP.Register_Button_Pot_Callback(E_POT_1, E_BUTTON_LEFT, E_BUTTON_LONG_PUSH_PRESSING, fCallback_Pot1_LeftButton_LongPush_Press, &mixer1);
  (void)OEP.Register_Button_Pot_Callback(E_POT_4, E_BUTTON_LEFT, E_BUTTON_LONG_PUSH_PRESSING, fCallback_Pot4_LeftButton_LongPush_Press, &mixer2);

	// Initial read of pots and configuration of led accordingly
	led_up       = map(OEP.Pot1_Value(), 0, 1023, 0, MAX_LED_VAL);
	led_left     = map(OEP.Pot2_Value(), 0, 1023, 0, MAX_LED_VAL);
	led_right    = map(OEP.Pot3_Value(), 0, 1023, 0, MAX_LED_VAL);

  OEP.SetLed(E_LED_UP, OEP_COLOR_BUILD(led_up, led_up, 0));
  OEP.SetLed(E_LED_LEFT, OEP_COLOR_BUILD(led_left, led_left, led_left));
  OEP.SetLed(E_LED_RIGHT, OEP_COLOR_BUILD(0, 0, led_right));

 iRet = OEP_MixIO.setup();
  if (iRet != 0)
  {
    serial_manager.print("OEP_MixIO setup failed: ");
    serial_manager.println(iRet);
  }

	AudioMemory(100);

	mixer1.gain(0, 0.5);
	mixer1.gain(1, 0.0);

  mixer2.gain(0, 0.0);
  mixer2.gain(1, 0.5);

  // we're done with the setup
	serial_manager.println("Open Effects Project - Test Application v1.0");
}



void loop()
{
	if(serial_manager.onReceive())
	{
	    if(serial_manager.isCmd("info"))
	    {
	    	/*reg_val = i2c_reg_read(SGTL5000_I2C_ADDR, CHIP_ID);
	    	serial_manager.print("CHIP_ID (PART, REV): (");
	    	//serial_manager.print((reg_val & 0xFF));
	    	serial_manager.print((reg_val >> 8) & 0xFF);
	    	serial_manager.print(", ");
	    	serial_manager.print(reg_val & 0xFF);
	    	serial_manager.println(")");*/
	    }
	}

  // run the Hardware Abstraction Layer routine
  // this will trigger the callback according to the user changes
  OEP.loop();

  OEP_MixIO.loop();
  
	delay(100);
}


/***************************************************
 * 
 *                CALLBACKS functions
 * 
 **************************************************/

void OEP_InputAbstraction::Pot1_Modified(int iNewPotValue)
{
  led_up = map(iNewPotValue, 0, 1023, 0, 128);

  OEP.SetLed(E_LED_UP, OEP_COLOR_BUILD(led_up, led_up, 0));
  OEP.PrintTextVal("Pot1", led_up, 0);

  serial_manager.print("Pot1 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::Pot2_Modified(int iNewPotValue)
{
  led_left = map(iNewPotValue, 0, 1023, 0, 128);
  if (OEP.ButtonLeft_State() != E_BUTTON_OFF)
  {
    OEP.SetLed(E_LED_LEFT, OEP_COLOR_BUILD(led_left, led_left, led_left));
  }
  OEP.PrintTextVal("Pot2", led_left, 0);

  serial_manager.print("Pot2 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::Pot3_Modified(int iNewPotValue)
{
  led_right = map(iNewPotValue, 0, 1023, 0, 128);
  if (OEP.ButtonLeft_State() != E_BUTTON_OFF)
  {
    OEP.SetLed(E_LED_RIGHT, OEP_COLOR_BUILD(0, 0, led_right));
  }
  OEP.PrintTextVal("Pot3", led_right, 0);

  serial_manager.print("Pot3 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::Pot4_Modified(int iNewPotValue)
{
  uint8_t iLevel = (uint8_t)map(iNewPotValue, 0, 1023, 0, 255);
  led_others = map(iNewPotValue, 0, 1023, 0, 128);

  OEP.SetBarGraph(iLevel);
  OEP.PrintTextVal("Pot4", led_others, 0);

  serial_manager.print("Pot4 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::Expr1_Modified(int iNewPotValue)
{
  serial_manager.print("Expr1 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::Expr2_Modified(int iNewPotValue)
{
  serial_manager.print("Expr2 = ");
  serial_manager.println(iNewPotValue);
}

void OEP_InputAbstraction::ButtonLeft_Latch_Modified(E_BUTTON_STATE eNewLatchState)
{
  if (eNewLatchState == E_BUTTON_OFF)
  {
    OEP.SetLed(E_LED_LEFT, OEP_COLOR_BUILD(0, 0, 0));
    OEP.SetBypassMode(E_BYPASS_ACTIVE);
    serial_manager.print("Bypass active");
  }
  else
  {
    OEP.SetLed(E_LED_LEFT, OEP_COLOR_BUILD(led_left, led_left, led_left));
    OEP.SetBypassMode(E_BYPASS_EFFECT);
    serial_manager.print("Effect active");
  }

  serial_manager.print("ButtonLatchLeft = ");
  serial_manager.println(eNewLatchState);
}

void OEP_InputAbstraction::ButtonRight_Modified(E_BUTTON_STATE eNewState)
{
  if (eNewState == E_BUTTON_OFF)
  {
    OEP.SetLed(E_LED_RIGHT, OEP_COLOR_BUILD(0, 0, 0));
  }
  else
  {
    OEP.SetLed(E_LED_RIGHT, OEP_COLOR_BUILD(0, 0, led_right));
  }

  serial_manager.print("ButtonRight = ");
  serial_manager.println(eNewState);
}

void OEP_InputAbstraction::SwitchLeft_Modified(E_SWITCH_STATE eNewState)
{
  serial_manager.print("SwitchLeft = ");
  serial_manager.println(eNewState);   
}

void OEP_InputAbstraction::SwitchRight_Modified(E_SWITCH_STATE eNewState)
{
  switch (eNewState)
  {
    case E_SWITCH_STATE_LEFT:
      // exit bargraph mode
      OEP.SetBarGraphMode(false);
      break; 
    case E_SWITCH_STATE_MIDDLE:
      {
        T_Color tColors[NUM_LEDS_BARGRAPH] = {0x42F45F, 0x5AF442, 0x9EF442, 0xCBF442, 0xF4D742, 0xF47D42, 0xF44242};
        uint8_t iLevel = (uint8_t)map(OEP.Pot4_Value(), 0, 1023, 0, 255);
        
        OEP.SetBarGraphColor(tColors);
        OEP.SetBarGraph(iLevel);
      }
      OEP.SetBarGraphMode(true);
      break; 
    case E_SWITCH_STATE_RIGHT:
      {
        T_Color tColors[NUM_LEDS_BARGRAPH] = {0x001456, 0x2247C1, 0x446AE5, 0x7C9BFF, 0xADC0FF, 0xC9D5FC, 0xFFFFFF};
        uint8_t iLevel = (uint8_t)map(OEP.Pot4_Value(), 0, 1023, 0, 255);
        
        OEP.SetBarGraphColor(tColors);
        OEP.SetBarGraph(iLevel);
      }
      OEP.SetBarGraphMode(true);
      break;
  }

  serial_manager.print("SwitchRight = ");
  serial_manager.println(eNewState);
}

int fCallback_LeftButton_LongPush_Press(void *pUserData)
{
  serial_manager.println("Left button long push pressing");
  
  return 0;
}

int fCallback_LeftButton_LongPush_Release(void *pUserData)
{
  serial_manager.println("Left button long push released");
  
  return 0;
}

int fCallback_Pot1_SwitchL_StateLeft(void *pUserData, int iNewPotValue)
{
  serial_manager.print("Pot1 with SwitchL set left: ");
  serial_manager.print(iNewPotValue);
  serial_manager.print("  udata = ");
  serial_manager.println((uint32_t)pUserData);
  
  return 0;
}

int fCallback_Pot2_SwitchL_StateMiddle(void *pUserData, int iNewPotValue)
{
  serial_manager.print("Pot2 with SwitchL set middle: ");
  serial_manager.print(iNewPotValue);
  serial_manager.print("  udata = ");
  serial_manager.println((uint32_t)pUserData);
  
  return 0;
}

int fCallback_Pot3_SwitchL_StateRight(void *pUserData, int iNewPotValue)
{
  serial_manager.print("Pot3 with SwitchL set right: ");
  serial_manager.print(iNewPotValue);
  serial_manager.print("  udata = ");
  serial_manager.println((uint32_t)pUserData);
  
  return 0;
}

int fCallback_Pot1_LeftButton_LongPush_Press(void *pUserData, int iNewPotValue)
{
  serial_manager.print("Set input volume: ");
  serial_manager.print(iNewPotValue);

  if (pUserData == &mixer1)
  {
    serial_manager.println("  udata = mixer1");
  }
  else if (pUserData == &mixer2)
  {
    serial_manager.println("  udata = mixer2");
  }
  else
  {
    serial_manager.println("  udata = unknown");
  }
  
  return 0;
}

int fCallback_Pot4_LeftButton_LongPush_Press(void *pUserData, int iNewPotValue)
{
  serial_manager.print("Set output volume: ");
  serial_manager.print(iNewPotValue);

  if (pUserData == &mixer1)
  {
    serial_manager.println("  udata = mixer1");
  }
  else if (pUserData == &mixer2)
  {
    serial_manager.println("  udata = mixer2");
  }
  else
  {
    serial_manager.println("  udata = unknown");
  }

  return 0;
}

