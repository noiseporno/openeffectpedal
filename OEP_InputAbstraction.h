/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_InputAbstraction__h__
#define __OEP_InputAbstraction__h__


#include "Arduino.h"
#include "OEP_Defines.h"


class OEP_InputAbstraction;


class OEP_Pot
{
  public:

	/*********************************
	*              Types             *
	*********************************/
	
	/*********************************
	*           Variables            *
	*********************************/
	E_POT					ePot;
	int 					iValue;

	/*********************************
	*         Main functions         *
	*********************************/
	OEP_Pot(OEP_InputAbstraction *pParent, E_POT ePot);
	~OEP_Pot(void);

	int  setup(void);
	void loop(void);
	boolean EvaluateCombi_ButtonPot(void);
	boolean EvaluateCombi_SwitchPot(void);
	
	/*********************************
	*        Control functions       *
	*********************************/
	void SetPin(int iPin);

	
  private:

	// HW related
	int 					iPin;

	// state
	OEP_InputAbstraction*	pParent;
	int 					iValuePrevious;
	
	int						AcquireValue(void);
};

class OEP_Switch
{
  public:

	/*********************************
	*              Types             *
	*********************************/
	
	/*********************************
	*           Variables            *
	*********************************/
	E_SWITCH				eSwitch;
	E_SWITCH_STATE			eState;

	/*********************************
	*         Main functions         *
	*********************************/
	OEP_Switch(OEP_InputAbstraction *pParent, E_SWITCH eSwitch, boolean bInverted);
	~OEP_Switch(void);

	int  setup(void);
	void loop(void);
	boolean EvaluateCombi_ButtonSwitch(void);

	/*********************************
	*        Control functions       *
	*********************************/
	void SetPin(int iPin);

	
  private:

	// HW related
	int 					iPin;
	boolean 				bInverted;

	// state
	OEP_InputAbstraction*	pParent;
	E_SWITCH_STATE			eStatePrevious;
	
	E_SWITCH_STATE			AcquireState(void);
};

class OEP_Button
{
  public:

	/*********************************
	*              Types             *
	*********************************/
	enum E_LP_STATE
	{
		E_LP_STATE_OFF = 0,
		E_LP_STATE_EVALUATE,
		E_LP_STATE_PRESS,
	};
	
	/*********************************
	*           Variables            *
	*********************************/
	E_BUTTON				eButton;
	E_BUTTON_STATE			eState;
	E_BUTTON_STATE			eLatchState;
	E_LP_STATE				eLongPress_State;

	/*********************************
	*         Main functions         *
	*********************************/
	OEP_Button(OEP_InputAbstraction *pParent, E_BUTTON eButton);
	~OEP_Button(void);

	int  setup(void);
	void loop(void);

	boolean IsLongPressed(void) {return (eLongPress_State == E_LP_STATE_PRESS);};

	/*********************************
	*        Control functions       *
	*********************************/
	void SetPin(int iPin);
	void SetLongPressTime(uint32_t iTimeInMs);

	
  private:

	// HW related
	int 					iPin;

	// state
	OEP_InputAbstraction*	pParent;
	E_BUTTON_STATE			eStatePrevious;
	uint32_t				iLongPress_TimeRef;
	uint32_t				iLongPress_Interval;
	void* 					pBounce = NULL;
	E_BUTTON_STATE			AcquireState(void);
};



class OEP_InputAbstraction
{
	friend class OEP_Pot;
	friend class OEP_Switch;
	friend class OEP_Button;
	
public:

	/*********************************
	*              Types             *
	*********************************/
	typedef int (*fCallback_Button)(void *pUserData);
	typedef int (*fCallback_Button_Pot)(void *pUserData, int iNewPotValue);
	typedef int (*fCallback_Switch_Pot)(void *pUserData, int iNewPotValue);
	typedef int (*fCallback_Button_Switch)(void *pUserData);


	/*********************************
	*           Variables            *
	*********************************/


	/*********************************
	*         Main functions         *
	*********************************/
	OEP_InputAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType);
	~OEP_InputAbstraction(void);

	int  setup(void);
	void loop(void);

	/*********************************
	*     Value accessfunctions      *
	*********************************/
	int Pot1_Value(void) {return Pot1.iValue;};
	int Pot2_Value(void) {return Pot2.iValue;};
	int Pot3_Value(void) {return Pot3.iValue;};
	int Pot4_Value(void) {return Pot4.iValue;};
	int	Expr1_Value(void) {return Expr1.iValue;};
	int	Expr2_Value(void) {return Expr2.iValue;};

	E_SWITCH_STATE SwitchLeft_State(void) {return SwitchLeft.eState;};
	E_SWITCH_STATE SwitchRight_State(void) {return SwitchRight.eState;};

	E_BUTTON_STATE ButtonLeft_State(void) {return ButtonLeft.eState;};
	E_BUTTON_STATE ButtonRight_State(void) {return ButtonRight.eState;};

	E_BUTTON_STATE ButtonLeft_LatchState(void) {return ButtonLeft.eLatchState;};
	E_BUTTON_STATE ButtonRight_LatchState(void) {return ButtonRight.eLatchState;};

	boolean ButtonLeft_IsLongPressed(void) {return ButtonLeft.IsLongPressed();};
	boolean ButtonRight_IsLongPressed(void) {return ButtonRight.IsLongPressed();};

	/*********************************
	*        Control functions       *
	*********************************/
	void SetThresholdPot(int iThreshold);
	void SetThresholdExpr(int iThreshold);
	void DisableExpr1(void);
	void DisableExpr2(void);
	void SetLongPressTime(E_BUTTON eButton, uint32_t iTimeInMs);

	/*********************************
	*            Callbacks           *
	*********************************/
	// callbacks when potentiometer is modified
	void Pot1_Modified(int iNewPotValue) __attribute__((weak));
	void Pot2_Modified(int iNewPotValue) __attribute__((weak));
	void Pot3_Modified(int iNewPotValue) __attribute__((weak));
	void Pot4_Modified(int iNewPotValue) __attribute__((weak));
	void Expr1_Modified(int iNewCvValue) __attribute__((weak));
	void Expr2_Modified(int iNewCvValue) __attribute__((weak));

	// callbacks when switch is modified
	void SwitchLeft_Modified(E_SWITCH_STATE eNewState) __attribute__((weak));
	void SwitchRight_Modified(E_SWITCH_STATE eNewState) __attribute__((weak));

	// callbacks when button is modified
	void ButtonLeft_Modified(E_BUTTON_STATE eNewState) __attribute__((weak));
	void ButtonRight_Modified(E_BUTTON_STATE eNewState) __attribute__((weak));
	void ButtonLeft_Latch_Modified(E_BUTTON_STATE eNewLatchState) __attribute__((weak));
	void ButtonRight_Latch_Modified(E_BUTTON_STATE eNewLatchState) __attribute__((weak));

	/*********************************
	*     Callback registering       *
	*********************************/

	// combination callbacks handling
	boolean Register_Button_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button fCallback, void *pUserData);
	boolean Register_Button_Pot_Callback(E_POT ePot, E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button_Pot fCallback, void *pUserData);	// does not support E_BUTTON_LONG_PUSH_RELEASE
	boolean Register_Switch_Pot_Callback(E_POT ePot, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Switch_Pot fCallback, void *pUserData);
	boolean Register_Button_Switch_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Button_Switch fCallback, void *pUserData);

	boolean Unregister_Button_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button fCallback, void *pUserData);
	boolean Unregister_Button_Pot_Callback(E_POT ePot, E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button_Pot fCallback, void *pUserData);
	boolean Unregister_Switch_Pot_Callback(E_POT ePot, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Switch_Pot fCallback, void *pUserData);
	boolean Unregister_Button_Switch_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Button_Switch fCallback, void *pUserData);

	void print_callback(void);
	
  
private:

	// HW related
	OEP_Pot			Pot1;
	OEP_Pot			Pot2;
	OEP_Pot			Pot3;
	OEP_Pot			Pot4;
	OEP_Pot			Expr1;
	OEP_Pot			Expr2;
	
	OEP_Switch		SwitchLeft;
	OEP_Switch		SwitchRight;
	
	OEP_Button		ButtonLeft;
	OEP_Button		ButtonRight;
	
	boolean			Expr1_Enable;
	boolean			Expr2_Enable;

	// thresholds
	int				VariationThreshold_Pot;
	int				VariationThreshold_Expr;
	
	// callback storage
	fCallback_Button		*fCallbacks_Button;
	fCallback_Button_Pot	*fCallbacks_Button_Pot;
	fCallback_Switch_Pot	*fCallbacks_Switch_Pot;
	fCallback_Button_Switch	*fCallbacks_Button_Switch;
	void*					*fCallbacks_Button_UserData;
	void*					*fCallbacks_Button_Pot_UserData;
	void*					*fCallbacks_Switch_Pot_UserData;
	void*					*fCallbacks_Button_Switch_UserData;

	
	void PotModified(E_POT ePot, int iNewPotValue);
	void SwitchModified(E_SWITCH eSwitch, E_SWITCH_STATE eNewState);
	void ButtonModified(E_BUTTON eButton, E_BUTTON_STATE eNewState);
	void ButtonLatchModified(E_BUTTON eButton, E_BUTTON_STATE eNewState);
	
	OEP_Pot*    GetInstance_Pot(E_POT ePot);
	OEP_Switch* GetInstance_Switch(E_SWITCH eSwitch);
	OEP_Button* GetInstance_Button(E_BUTTON eButton);
};


#endif
