/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "Arduino.h"
#include "OEP_Abstraction.h"

OEP_Abstraction::OEP_Abstraction(E_MB_Type eMbType, E_EXT_Type eExtType) :
	OEP_InputAbstraction(eMbType, eExtType), OEP_OutputAbstraction(eMbType, eExtType), OEP_AudioAbstraction(eMbType, eExtType), OEP_StorageAbstraction(eMbType, eExtType)
{
  eMotherboardType = eMbType;
  eExtensionType = eExtType;
}

OEP_Abstraction::~OEP_Abstraction(void)
{
}

int OEP_Abstraction::setup(void)
{
	int iRet = 0;
	
	iSetupRet_InputAbstraction = OEP_InputAbstraction::setup();
	if (iSetupRet_InputAbstraction != 0) iRet |= 0x0001;
	
	iSetupRet_OutputAbstraction = OEP_OutputAbstraction::setup();
	if (iSetupRet_OutputAbstraction != 0) iRet |= 0x0002;
	
	iSetupRet_AudioAbstraction = OEP_AudioAbstraction::setup();
	if (iSetupRet_AudioAbstraction != 0) iRet |= 0x0004;
	
	return iRet;
}

void OEP_Abstraction::loop(void)
{
	OEP_InputAbstraction::loop();
	OEP_OutputAbstraction::loop();
	OEP_AudioAbstraction::loop();
}