/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "OEP_AudioAbstraction.h"
#include <Wire.h>


/*********************************
*             Defines            *
*********************************/
#define PIN_RELAY_L			4     // select the pin for the RelayL
#define PIN_RELAY_R			5     // select the pin for the RelayR

#define SGTL5000_I2C_ADDR	0x0A  // CTRL_ADR0_CS pin low (normal configuration)

#define CHIP_ID				0x0000
#define CHIP_DIG_POWER      0x0002
#define CHIP_CLK_CTRL       0x0004
#define CHIP_I2S_CTRL       0x0006
#define CHIP_SSS_CTRL       0x000A
#define CHIP_ADCDAC_CTRL    0x000E
#define CHIP_DAC_VOL        0x0010
#define CHIP_ANA_ADC_CTRL   0x0020



/*********************************
*           Variables            *
*********************************/
AudioControlSGTL5000    	sgtl5000;


/*********************************
*         Main functions         *
*********************************/
OEP_AudioAbstraction::OEP_AudioAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType)
{
	// HW dependent initialization
	Pin_RelayL = PIN_RELAY_L;
	Pin_RelayR = PIN_RELAY_R;

	// Configure relays
	pinMode(Pin_RelayL, OUTPUT);
	pinMode(Pin_RelayR, OUTPUT);
}

OEP_AudioAbstraction::~OEP_AudioAbstraction(void)
{
}

int OEP_AudioAbstraction::setup(void)
{
	// disable effect path for now
	SetBypassMode(E_BYPASS_ACTIVE);

	// Setup the audio path
	sgtl5000.setAddress(LOW);
	sgtl5000.enable();  						// Enable the audio shield
	delay(100);
	sgtl5000.inputSelect(AUDIO_INPUT_LINEIN);	// ADC->I2S, I2S->DAC
	sgtl5000.lineInLevel(0);   					// set to 3.12Vp-p
	sgtl5000.lineOutLevel(13); 					// set to 3.16Vp-p
	sgtl5000.dacVolume(1.0f);					// 0dB (range: -90dB..0dB)
	
	return 0;
}

void OEP_AudioAbstraction::loop(void)
{
}


/*********************************
*      Bypass access functions   *
*********************************/
void OEP_AudioAbstraction::SetBypassMode(E_BYPASS_MODE eMode)
{
	if (eMode == E_BYPASS_ACTIVE)
	{
	  digitalWrite(Pin_RelayL, LOW);
	  digitalWrite(Pin_RelayR, LOW);
	}
	else
	{
	  digitalWrite(Pin_RelayL, HIGH);
	  digitalWrite(Pin_RelayR, HIGH);
	}	
}

/*********************************
*         Volume functions       *
*********************************/
void OEP_AudioAbstraction::SetInputVolume(float Vol_inDB)
{
	
}

void OEP_AudioAbstraction::SetOutputVolume(float Vol_inDB)
{
	
}


/*********************************
*      Codec access function     *
*********************************/
unsigned int  OEP_AudioAbstraction::i2c_reg_read(uint8_t i2c_addr, unsigned int reg)
{
	unsigned int val;
	Wire.beginTransmission(i2c_addr);
	Wire.write(reg >> 8);
	Wire.write(reg);
	if (Wire.endTransmission(false) != 0) return 0;
	if (Wire.requestFrom((int)i2c_addr, 2) < 2) return 0;
	val = Wire.read() << 8;
	val |= Wire.read();
	return val;
}

