/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_Defines__h__
#define __OEP_Defines__h__


#include "Arduino.h"

/************************************************
*  Hardware definition types
************************************************/
enum E_MB_Type
{
  E_MB_Type_Rev2_1 = 0x0201
};

enum E_EXT_Type
{
  E_EXT_Type_2MI_2MO = 0,
  E_EXT_Type_2MI_1EX_1MO = 1,
  E_EXT_Type_1SI_2EX_1SO = 2
};


/*************************************************
* UI input elements typedef
*************************************************/
// !!! Developper WARNING!!!
// The following enums must start at 0 and be consecutive
// If a new value is to be added, the .cpp file needs update to the max value
// These constrains are used in the .cpp to build combined values
// !!! Developper WARNING !!!
enum E_POT
{
	E_POT_1  = 0,
	E_POT_2  = 1,
	E_POT_3  = 2,
	E_POT_4  = 3,
	E_EXPR_1 = 4,
	E_EXPR_2 = 5,
	// before modification, see warning above
};

enum E_BUTTON
{
	E_BUTTON_LEFT  = 0,
	E_BUTTON_RIGHT = 1,
	// before modification, see warning above
};

enum E_BUTTON_STATE
{
	E_BUTTON_OFF = 0,
	E_BUTTON_ON = 1,
	// before modification, see warning above
};

enum E_BUTTON_ACTION
{
	E_BUTTON_LONG_PUSH_PRESSING = 0,
	E_BUTTON_LONG_PUSH_RELEASE = 1,
	// before modification, see warning above
};

enum E_SWITCH
{
	E_SWITCH_LEFT  = 0,
	E_SWITCH_RIGHT = 1,
	// before modification, see warning above
};

enum E_SWITCH_STATE
{
	E_SWITCH_STATE_LEFT   = 0,
	E_SWITCH_STATE_MIDDLE = 1,
	E_SWITCH_STATE_RIGHT  = 2,
	// before modification, see warning above
};


/*************************************************
* UI output elements typedef
*************************************************/
#define OEP_COLOR_BUILD(Red, Green, Blue)	(T_Color)(((uint32_t)Red << 16) | ((uint32_t)Green << 8) | ((uint32_t)Blue))
#define OEP_COLOR_EXTRACT_RED(tColor)		(uint8_t)((uint32_t)(tColor >> 16) & 0xFF)
#define OEP_COLOR_EXTRACT_GREEN(tColor)		(uint8_t)(((uint32_t)tColor >> 8) & 0xFF)
#define OEP_COLOR_EXTRACT_BLUE(tColor)		(uint8_t)(((uint32_t)tColor) & 0xFF)


enum E_LED
{
	E_LED_UP = 0,
	E_LED_RIGHT = 1,
	E_LED_LEFT = 2,
	E_LED_1 = 3,
	E_LED_2 = 4,
	E_LED_3 = 5,
	E_LED_4 = 6,
	E_LED_5 = 7,
	E_LED_6 = 8,
	E_LED_7 = 9,
};

typedef uint32_t T_Color;


/*************************************************
* Audio elements typedef
*************************************************/
enum E_BYPASS_MODE
{
	E_BYPASS_ACTIVE = 0,
	E_BYPASS_EFFECT = 1,
};


#endif
