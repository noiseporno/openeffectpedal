/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_AudioAbstraction__h__
#define __OEP_AudioAbstraction__h__


#include "Arduino.h"
#include "OEP_Defines.h"
#include <Audio.h>


class OEP_AudioAbstraction
{
public:

	/*********************************
	*              Types             *
	*********************************/
	
	/*********************************
	*           Variables            *
	*********************************/

  
  
	/*********************************
	*         Main functions         *
	*********************************/
	OEP_AudioAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType);
	~OEP_AudioAbstraction(void);

	int  setup(void);
	void loop(void);

    
	/*********************************
	*      Bypass access functions   *
	*********************************/
	void    SetBypassMode(E_BYPASS_MODE eMode);

	
	/*********************************
	*         Volume functions       *
	*********************************/
	void	SetInputVolume(float Vol_inDB);
	void	SetOutputVolume(float Vol_inDB);

	
	/*********************************
	*      Codec access function     *
	*********************************/

	
	
private:

	int		Pin_RelayL;
	int		Pin_RelayR;

	unsigned int  i2c_reg_read(uint8_t i2c_addr, unsigned int reg);
	
};


#endif
