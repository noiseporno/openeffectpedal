# OpenEffect Pedal library #

## Purpose ##
This library serves as a Hardware Abstraction Layer to the OpenEffet pedal.

Helper functions & callbacks are provided to write lighter applications focusing on applicative tasks.

Applicative helper class are provided as well to handle complex tasks involving the UI.
The first allows to handle the codec levels and settings and save the configuration.
The second allows to handle input and output mixing between Audio and USB Audio path and save the configuration.

## Howto use ##

The library should be installed next to the Audio library in the Arduino\hardware\teensy\avr\libraries directory.

### Follow these steps to use the library: ###
1. Include "OpenEffectPedal.h" file in your application.
1. Instantiate the OEP_Abstraction class and provide it with the HW details
1. Add a call to the class setup (and loop) function in the setup (and loop) application function.
1. Add callbacks as needed in the application. For example:

```
#!c++

OEP_InputAbstraction OEP;

void setup()
{
  // add your code here
  
  int iRet = OEP.setup();
  if (iRet != 0)
  {
    // handle error here
  }
  
  // add your code here
}

void loop()
{
  // add your code here
  
  OEP.loop();
}

void OEP_InputAbstraction::Pot1_Modified(int iNewPotValue)
{
... add your code here
}
```

Some additionnal combination callbacks can be registered through specific functions.

## OEP_APP_ExtLevels Applicative helper class usage ##

The OEP_APP_ExtLevels class is to be used in the same way as OEP does: instantiation, setup, loop.

A long press on the left tap button (if configured on the left button) enters the setting mode.
When that is done, the left switch is used to select which function is to be set:
- left position: set levels
- middle position: set EQ parameters
- right position: set AVC parameters

When setting levels, the following controls can be used:
- POT_1 sets input volume
- POT_2 sets dac level
- POT_3 sets highpass state
- POT_4 sets output level 

When setting EQ parameters, the following controls can be used:
- Right switch, left position : no EQ
- Right switch, middle position : Tone control
  * POT_1 sets bass level
  * POT_4 sets treble level
- Right switch, right position : 5-bands EQ
  * POT_1 selects which band is active
  * POT_4 sets the level of the selected band
  
When setting AGC parameters, the following controls can be used:
- Right switch, left position : no AVC
- Right switch, middle position : AVC enabled, level-based parameters
  * POT_1 sets max Gain (0.0, 6.0 or 12.0 dB)
  * POT_2 sets the threshold (0.0dB to -96dB)
  * POT_3 sets the hard limiter state (enabled/disabled)
- Right switch, right position : AVC enabled, time-based parameters
  * POT_1 sets the integrator response time (0, 25, 50 or 100ms)
  * POT_2 sets the attack rate in dB/s
  * POT_3 sets the release rate in dB/s


## Contact ##

Contact the NoisePorno team for any question, issue, remark, ...