/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "OEP_InputAbstraction.h"
#include <Bounce.h>
#include <elapsedMillis.h>

#include <RBD_SerialManager.h>
extern RBD::SerialManager 			serial_manager;

// uncomment the following to enable logging of callback information
//#define LOG_CALLBACK_INFO


/*********************************
*             Defines            *
*********************************/
#define E_POT_MAX 				((uint8_t)E_EXPR_2 + 1)
#define E_BUTTON_MAX			((uint8_t)E_BUTTON_RIGHT + 1)
#define E_BUTTON_STATE_MAX		((uint8_t)E_BUTTON_ON + 1)
#define E_BUTTON_ACTION_MAX		((uint8_t)E_BUTTON_LONG_PUSH_RELEASE + 1)
#define E_SWITCH_MAX			((uint8_t)E_SWITCH_RIGHT + 1)
#define E_SWITCH_STATE_MAX		((uint8_t)E_SWITCH_STATE_RIGHT + 1)

#define CBID_POT_BUILD(ePot)											((uint8_t)(ePot))
#define CBID_POT_1														CBID_POT_BUILD(E_POT_1)
#define CBID_POT_2														CBID_POT_BUILD(E_POT_2)
#define CBID_POT_3														CBID_POT_BUILD(E_POT_3)
#define CBID_POT_4														CBID_POT_BUILD(E_POT_4)
#define CBID_EXPR_1														CBID_POT_BUILD(E_EXPR_1)
#define CBID_EXPR_2														CBID_POT_BUILD(E_EXPR_2)
#define CBID_POT_MAX													CBID_POT_BUILD(E_POT_MAX)

#define CBID_BUTTON_BUILD(eButton, eButtonAction)						((((uint8_t)E_BUTTON_ACTION_MAX) * (uint8_t)eButton) + (uint8_t)eButtonAction)
#define CBID_BUTTON_LEFT_LONGPUSH_PRESSING								CBID_BUTTON_BUILD(E_BUTTON_LEFT,  E_BUTTON_LONG_PUSH_PRESSING)
#define CBID_BUTTON_LEFT_LONGPUSH_RELEASE								CBID_BUTTON_BUILD(E_BUTTON_LEFT,  E_BUTTON_LONG_PUSH_RELEASE )
#define CBID_BUTTON_RIGHT_LONGPUSH_PRESSING								CBID_BUTTON_BUILD(E_BUTTON_RIGHT, E_BUTTON_LONG_PUSH_PRESSING)
#define CBID_BUTTON_RIGHT_LONGPUSH_RELEASE								CBID_BUTTON_BUILD(E_BUTTON_RIGHT, E_BUTTON_LONG_PUSH_RELEASE )
#define CBID_BUTTON_MAX					 								(CBID_BUTTON_BUILD((E_BUTTON_MAX - 1), (E_BUTTON_ACTION_MAX - 1)) + 1)

#define CBID_BUTTON_POT_BUILD(eButton, ePot)							((((uint8_t)E_POT_MAX) * (uint8_t)eButton) + (uint8_t)ePot)
#define CBID_BUTTON_LEFT_POT1											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_POT_1 )
#define CBID_BUTTON_LEFT_POT2											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_POT_2 )
#define CBID_BUTTON_LEFT_POT3											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_POT_3 )
#define CBID_BUTTON_LEFT_POT4											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_POT_4 )
#define CBID_BUTTON_LEFT_EXPR1											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_EXPR_1)
#define CBID_BUTTON_LEFT_EXPR2											CBID_BUTTON_POT_BUILD(E_BUTTON_LEFT,  E_EXPR_2) 
#define CBID_BUTTON_RIGHT_POT1											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_POT_1 )
#define CBID_BUTTON_RIGHT_POT2											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_POT_2 )
#define CBID_BUTTON_RIGHT_POT3											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_POT_3 )
#define CBID_BUTTON_RIGHT_POT4											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_POT_4 )
#define CBID_BUTTON_RIGHT_EXPR1											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_EXPR_1) 
#define CBID_BUTTON_RIGHT_EXPR2											CBID_BUTTON_POT_BUILD(E_BUTTON_RIGHT, E_EXPR_2) 
#define CBID_BUTTON_POT_MAX												(CBID_BUTTON_POT_BUILD((E_BUTTON_MAX - 1), (E_POT_MAX - 1)) + 1)

#define CBID_SWITCH_POT_BUILD(eSwitch, ePot, eSwitchState)				(((uint8_t)E_SWITCH_STATE_MAX * ((((uint8_t)E_POT_MAX) * (uint8_t)eSwitch) + (uint8_t)ePot)) + (uint8_t)eSwitchState)
#define CBID_SWITCH_LEFT_POT1_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_1 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_POT1_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_1 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_POT1_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_1 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_LEFT_POT2_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_2 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_POT2_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_2 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_POT2_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_2 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_LEFT_POT3_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_3 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_POT3_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_3 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_POT3_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_3 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_LEFT_POT4_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_4 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_POT4_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_4 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_POT4_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_POT_4 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_LEFT_EXPR1_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_1, E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_EXPR1_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_1, E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_EXPR1_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_1, E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_LEFT_EXPR2_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_2, E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_LEFT_EXPR2_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_2, E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_LEFT_EXPR2_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_LEFT , E_EXPR_2, E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_POT1_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_1 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_POT1_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_1 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_POT1_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_1 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_POT2_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_2 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_POT2_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_2 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_POT2_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_2 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_POT3_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_3 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_POT3_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_3 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_POT3_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_3 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_POT4_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_4 , E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_POT4_STATE_MIDDLE								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_4 , E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_POT4_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_POT_4 , E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_EXPR1_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_1, E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_EXPR1_STATE_MIDDLE							CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_1, E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_EXPR1_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_1, E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_RIGHT_EXPR2_STATE_LEFT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_2, E_SWITCH_STATE_LEFT  )
#define CBID_SWITCH_RIGHT_EXPR2_STATE_MIDDLE							CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_2, E_SWITCH_STATE_MIDDLE)
#define CBID_SWITCH_RIGHT_EXPR2_STATE_RIGHT								CBID_SWITCH_POT_BUILD(E_SWITCH_RIGHT, E_EXPR_2, E_SWITCH_STATE_RIGHT )
#define CBID_SWITCH_POT_MAX												(CBID_SWITCH_POT_BUILD((E_SWITCH_MAX - 1), (E_POT_MAX - 1), (E_SWITCH_STATE_MAX - 1)) + 1)

#define CBID_BUTTON_SWITCH_BUILD(eButton, eSwitch, eButtonAction, eSwitchState)	(((uint8_t)E_SWITCH_STATE_MAX * ((uint8_t)E_BUTTON_ACTION_MAX * ((uint8_t)E_SWITCH_MAX * (uint8_t)eButton + eSwitch) + eButtonAction)) + (uint8_t)eSwitchState)
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_MIDDLE				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_MIDDLE				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_LEFT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_MIDDLE			CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_MIDDLE				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_LEFT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_LEFT,  E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_MIDDLE			CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_PRESSING_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_MIDDLE				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_RIGHT_SWITCH_LEFT_LONGPUSH_RELEASE_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_LEFT,  E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_MIDDLE			CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_PRESSING_STATE_RIGHT			CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_LEFT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_LEFT  )
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_MIDDLE			CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_MIDDLE)
#define CBID_BUTTON_RIGHT_SWITCH_RIGHT_LONGPUSH_RELEASE_STATE_RIGHT				CBID_BUTTON_SWITCH_BUILD(E_BUTTON_RIGHT, E_SWITCH_RIGHT, E_BUTTON_LONG_PUSH_RELEASE,  E_SWITCH_STATE_RIGHT )
#define CBID_BUTTON_SWITCH_MAX					 								(CBID_BUTTON_SWITCH_BUILD((E_BUTTON_MAX - 1),  (E_SWITCH_MAX - 1), (E_BUTTON_ACTION_MAX - 1), (E_SWITCH_STATE_MAX - 1)) + 1)


/*********************************
*            Variables           *
*********************************/
elapsedMillis OEP_InputAbstraction_timeElapsed = 0;


/*********************************
*         Main functions         *
*********************************/
OEP_InputAbstraction::OEP_InputAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType) :
	Pot1(this, E_POT_1), Pot2(this, E_POT_2), Pot3(this, E_POT_3), Pot4(this, E_POT_4), Expr1(this, E_EXPR_1), Expr2(this, E_EXPR_2),
	SwitchLeft(this, E_SWITCH_LEFT, false), SwitchRight(this, E_SWITCH_RIGHT, true), ButtonLeft(this, E_BUTTON_LEFT), ButtonRight(this, E_BUTTON_RIGHT)
{
	int /*Pin_ButtonLeft, Pin_ButtonRight,*/ index;
	
	// HW specific assignments
	if (eMbType == E_MB_Type_Rev2_1)
	{
		Pot1.SetPin(A6);		 // select the input pin for the potentiometer 1
		Pot2.SetPin(A3);		 // select the input pin for the potentiometer 2
		Pot3.SetPin(A2);		 // select the input pin for the potentiometer 3
		Pot4.SetPin(A1);		 // select the input pin for the potentiometer 4
		Expr1.SetPin(A10);		 // select the input pin for the Expression 1
		Expr2.SetPin(A11);		 // select the input pin for the Expression 2
		SwitchLeft.SetPin(A12);	 // Toggle Switch Left
		SwitchRight.SetPin(A13); // Toggle Switch Right
		ButtonLeft.SetPin(2);	 // pin for the left foot switch
		ButtonRight.SetPin(3);	 // pin for the right foot switch
	}

	// set thresholds to default value
	VariationThreshold_Pot = 5;
	VariationThreshold_Expr = 5;

	switch(eExtType)
	{
		case E_EXT_Type_2MI_2MO:
			Expr1_Enable = false;
			Expr2_Enable = false;
			break;
		case E_EXT_Type_2MI_1EX_1MO:
			Expr1_Enable = true;
			Expr2_Enable = false;
			break;
		case E_EXT_Type_1SI_2EX_1SO:
			Expr1_Enable = true;
			Expr2_Enable = true;
			break;
	}

	fCallbacks_Button = new fCallback_Button[CBID_BUTTON_MAX];
	if (fCallbacks_Button != NULL)
	{
		for (index = 0; index < CBID_BUTTON_MAX; index++)
			fCallbacks_Button[index] = NULL;
	}
	fCallbacks_Button_Pot = new fCallback_Button_Pot[CBID_BUTTON_POT_MAX];
	if (fCallbacks_Button_Pot != NULL)
	{
		for (index = 0; index < CBID_BUTTON_POT_MAX; index++)
			fCallbacks_Button_Pot[index] = NULL;
	}
	fCallbacks_Switch_Pot = new fCallback_Switch_Pot[CBID_SWITCH_POT_MAX];
	if (fCallbacks_Switch_Pot != NULL)
	{
		for (index = 0; index < CBID_SWITCH_POT_MAX; index++)
			fCallbacks_Switch_Pot[index] = NULL;
	}
	fCallbacks_Button_Switch = new fCallback_Button_Switch[CBID_BUTTON_SWITCH_MAX];
	if (fCallbacks_Button_Switch != NULL)
	{
		for (index = 0; index < CBID_BUTTON_SWITCH_MAX; index++)
			fCallbacks_Button_Switch[index] = NULL;
	}
	
	fCallbacks_Button_UserData = new void*[CBID_BUTTON_MAX];
	if (fCallbacks_Button_UserData != NULL)
	{
		for (index = 0; index < CBID_BUTTON_MAX; index++)
			fCallbacks_Button_UserData[index] = NULL;
	}
	fCallbacks_Button_Pot_UserData = new void*[CBID_BUTTON_POT_MAX];
	if (fCallbacks_Button_Pot_UserData != NULL)
	{
		for (index = 0; index < CBID_BUTTON_POT_MAX; index++)
			fCallbacks_Button_Pot_UserData[index] = NULL;
	}
	fCallbacks_Switch_Pot_UserData = new void*[CBID_SWITCH_POT_MAX];
	if (fCallbacks_Switch_Pot_UserData != NULL)
	{
		for (index = 0; index < CBID_SWITCH_POT_MAX; index++)
			fCallbacks_Switch_Pot_UserData[index] = NULL;
	}
	fCallbacks_Button_Switch_UserData = new void*[CBID_BUTTON_SWITCH_MAX];
	if (fCallbacks_Button_Switch_UserData != NULL)
	{
		for (index = 0; index < CBID_BUTTON_SWITCH_MAX; index++)
			fCallbacks_Button_Switch_UserData[index] = NULL;
	}
}

OEP_InputAbstraction::~OEP_InputAbstraction(void)
{
	if (fCallbacks_Button != NULL)
		delete[] fCallbacks_Button;
	if (fCallbacks_Button_Pot != NULL)
		delete[] fCallbacks_Button_Pot;
	if (fCallbacks_Switch_Pot != NULL)
		delete[] fCallbacks_Switch_Pot;
	if (fCallbacks_Button_Switch != NULL)
		delete[] fCallbacks_Button_Switch;
	if (fCallbacks_Button_UserData != NULL)
		delete[] fCallbacks_Button_UserData;
	if (fCallbacks_Button_Pot_UserData != NULL)
		delete[] fCallbacks_Button_Pot_UserData;
	if (fCallbacks_Switch_Pot_UserData != NULL)
		delete[] fCallbacks_Switch_Pot_UserData;
	if (fCallbacks_Button_Switch_UserData != NULL)
		delete[] fCallbacks_Button_Switch_UserData;
}

int OEP_InputAbstraction::setup(void)
{
	int iRet = 0;
	
	OEP_InputAbstraction_timeElapsed = 0;
	
	if ((fCallbacks_Button == NULL) || (fCallbacks_Button_Pot == NULL) ||
		(fCallbacks_Switch_Pot == NULL) || (fCallbacks_Button_Switch == NULL) ||
		(fCallbacks_Button_UserData == NULL) || (fCallbacks_Button_Pot_UserData == NULL) ||
		(fCallbacks_Switch_Pot_UserData == NULL) || (fCallbacks_Button_Switch_UserData == NULL))
		iRet |= 0x0001;
	
	if (Pot1.setup() != 0) iRet |= 0x0002;
	if (Pot2.setup() != 0) iRet |= 0x0004;
	if (Pot3.setup() != 0) iRet |= 0x0008;
	if (Pot4.setup() != 0) iRet |= 0x0010;
	if (Expr1_Enable == true)
	{
		if (Expr1.setup() != 0) iRet |= 0x0020;
		
	}
	if (Expr2_Enable == true)
	{
		if (Expr2.setup() != 0) iRet |= 0x0040;
	}

	if (SwitchLeft.setup() != 0) iRet |= 0x0080;
	if (SwitchRight.setup() != 0) iRet |= 0x0100;
	
	if (ButtonLeft.setup() != 0) iRet |= 0x0200;
	if (ButtonRight.setup() != 0) iRet |= 0x0400;
	
	return iRet;
}

void OEP_InputAbstraction::print_callback(void)
{
	int index;
	
	serial_manager.print("CBID_BUTTON_MAX=");
	serial_manager.println(CBID_BUTTON_MAX);
	serial_manager.print("CBID_BUTTON_POT_MAX=");
	serial_manager.println(CBID_BUTTON_POT_MAX);
	serial_manager.print("CBID_SWITCH_POT_MAX=");
	serial_manager.println(CBID_SWITCH_POT_MAX);
	serial_manager.print("CBID_BUTTON_SWITCH_MAX=");
	serial_manager.println(CBID_BUTTON_SWITCH_MAX);
	
	serial_manager.println("Button:");
	for (index = 0; index < CBID_BUTTON_MAX; index++)
	{
		serial_manager.print((uint32_t)fCallbacks_Button[index]);
		serial_manager.print("  ");
	}
	serial_manager.println("  ");

	serial_manager.println("Button/Pot:");
	for (index = 0; index < CBID_BUTTON_POT_MAX; index++)
	{
		serial_manager.print((uint32_t)fCallbacks_Button_Pot[index]);
		serial_manager.print("  ");
	}
	serial_manager.println("  ");

	serial_manager.println("Switch/Pot:");
	for (index = 0; index < CBID_SWITCH_POT_MAX; index++)
	{
		serial_manager.print((uint32_t)fCallbacks_Switch_Pot[index]);
		serial_manager.print("  ");
	}
	serial_manager.println("  ");

	serial_manager.println("Button/Switch:");
	for (index = 0; index < CBID_BUTTON_SWITCH_MAX; index++)
	{
		serial_manager.print((uint32_t)fCallbacks_Button_Switch[index]);
		serial_manager.print("  ");
	}
	serial_manager.println("  ");
}

void OEP_InputAbstraction::loop(void)
{
#ifdef LOG_CALLBACK_INFO
	static int iPrintCallbackInfo = 0;
	if (iPrintCallbackInfo == 0)
	{
		iPrintCallbackInfo = 1;
		
		print_callback();
	}
#endif
	
	Pot1.loop();
	Pot2.loop();
	Pot3.loop();
	Pot4.loop();
	if (Expr1_Enable == true)
		Expr1.loop();
	if (Expr2_Enable == true)
		Expr2.loop();
	
	SwitchLeft.loop();
	SwitchRight.loop();
	
	ButtonLeft.loop();
	ButtonRight.loop();
}

OEP_Pot* OEP_InputAbstraction::GetInstance_Pot(E_POT ePot)
{
	switch(ePot)
	{
		case E_POT_1:
			return &Pot1;
		case E_POT_2:
			return &Pot2;
		case E_POT_3:
			return &Pot3;
		case E_POT_4:
			return &Pot4;
		case E_EXPR_1:
			return &Expr1;
		case E_EXPR_2:
			return &Expr2;
		default:
			return NULL;
	}
}

OEP_Switch* OEP_InputAbstraction::GetInstance_Switch(E_SWITCH eSwitch)
{
	switch (eSwitch)
	{
		case E_SWITCH_LEFT:
			return &SwitchLeft;
		case E_SWITCH_RIGHT:
			return &SwitchRight;
		default:
			return NULL;
	}
}

OEP_Button* OEP_InputAbstraction::GetInstance_Button(E_BUTTON eButton)
{
	switch (eButton)
	{
		case E_BUTTON_LEFT:
			return &ButtonLeft;
		case E_BUTTON_RIGHT:
			return &ButtonRight;
		default:
			return NULL;
	}
}



/*********************************
*        Control functions       *
*********************************/
void OEP_InputAbstraction::SetThresholdPot(int iThreshold)
{
	VariationThreshold_Pot = iThreshold;
}

void OEP_InputAbstraction::SetThresholdExpr(int iThreshold)
{
	VariationThreshold_Expr = iThreshold;
}

void OEP_InputAbstraction::DisableExpr1(void)
{
	Expr1_Enable = false;
}

void OEP_InputAbstraction::DisableExpr2(void)
{
	Expr2_Enable = false;
}

void OEP_InputAbstraction::SetLongPressTime(E_BUTTON eButton, uint32_t iTimeInMs)
{
	OEP_Button* pButton;
	
	pButton = GetInstance_Button(eButton);
	if (pButton != NULL)
	{
		pButton->SetLongPressTime(iTimeInMs);
	}
}

/*********************************
*            Callbacks           *
*********************************/
// callbacks when potentiometer is modified
void OEP_InputAbstraction::PotModified(E_POT ePot, int iNewPotValue)
{
	switch(ePot)
	{
		case E_POT_1:
			Pot1_Modified(iNewPotValue);
			break;
		case E_POT_2:
			Pot2_Modified(iNewPotValue);
			break;
		case E_POT_3:
			Pot3_Modified(iNewPotValue);
			break;
		case E_POT_4:
			Pot4_Modified(iNewPotValue);
			break;
		case E_EXPR_1:
			Expr1_Modified(iNewPotValue);
			break;
		case E_EXPR_2:
			Expr2_Modified(iNewPotValue);
			break;
		default:
			break;
	}
}

void OEP_InputAbstraction::Pot1_Modified(int iNewPotValue) {};
void OEP_InputAbstraction::Pot2_Modified(int iNewPotValue) {};
void OEP_InputAbstraction::Pot3_Modified(int iNewPotValue) {};
void OEP_InputAbstraction::Pot4_Modified(int iNewPotValue) {};
void OEP_InputAbstraction::Expr1_Modified(int iNewCvValue) {};
void OEP_InputAbstraction::Expr2_Modified(int iNewCvValue) {};

// callbacks when switch is modified
void OEP_InputAbstraction::SwitchModified(E_SWITCH eSwitch, E_SWITCH_STATE eNewState)
{
	switch (eSwitch)
	{
		case E_SWITCH_LEFT:
			SwitchLeft_Modified(eNewState);
			break;
		case E_SWITCH_RIGHT:
			SwitchRight_Modified(eNewState);
			break;
		default:
			break;
	}
}

void OEP_InputAbstraction::SwitchLeft_Modified(E_SWITCH_STATE eNewState) {};
void OEP_InputAbstraction::SwitchRight_Modified(E_SWITCH_STATE eNewState) {};

// callbacks when button is modified
void OEP_InputAbstraction::ButtonModified(E_BUTTON eButton, E_BUTTON_STATE eNewState)
{
	switch(eButton)
	{
		case E_BUTTON_LEFT:
			ButtonLeft_Modified(eNewState);
			break;
		case E_BUTTON_RIGHT:
			ButtonRight_Modified(eNewState);
			break;
		default:
			break;
	}
}

void OEP_InputAbstraction::ButtonLeft_Modified(E_BUTTON_STATE eNewState) {};
void OEP_InputAbstraction::ButtonRight_Modified(E_BUTTON_STATE eNewState) {};

// callbacks when button latch is modified
void OEP_InputAbstraction::ButtonLatchModified(E_BUTTON eButton, E_BUTTON_STATE eNewLatchState)
{
	switch(eButton)
	{
		case E_BUTTON_LEFT:
			ButtonLeft_Latch_Modified(eNewLatchState);
			break;
		case E_BUTTON_RIGHT:
			ButtonRight_Latch_Modified(eNewLatchState);
			break;
		default:
			break;
	}
}

void OEP_InputAbstraction::ButtonLeft_Latch_Modified(E_BUTTON_STATE eNewLatchState) {};
void OEP_InputAbstraction::ButtonRight_Latch_Modified(E_BUTTON_STATE eNewLatchState) {};

  
/*********************************
*     Callback registering       *
*********************************/

// combination callbacks handling
boolean OEP_InputAbstraction::Register_Button_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button fCallback, void *pUserData)
{
	uint8_t iCallbackIndex;

#ifdef LOG_CALLBACK_INFO
	serial_manager.println("Before Register_Button_Callback:");
	print_callback();	
#endif
	
	// make sure the callback array is allocated
	if ((fCallbacks_Button == NULL) || (fCallbacks_Button_UserData == NULL))
		return false;
		
	// sanity check on the input values
	if ((eButton >= E_BUTTON_MAX) || (eButtonAction > E_BUTTON_ACTION_MAX))
		return false;
	
	// compute the callback index we need to address
	iCallbackIndex = CBID_BUTTON_BUILD(eButton, eButtonAction);
	
	// sanity check on the computed index
	if (iCallbackIndex > CBID_BUTTON_MAX)
		return false;
		
	// assign callback
	fCallbacks_Button[iCallbackIndex] = fCallback;
	fCallbacks_Button_UserData[iCallbackIndex] = pUserData;

#ifdef LOG_CALLBACK_INFO
	serial_manager.println("After Register_Button_Callback:");
	print_callback();	
#endif
	
	return true;
}

boolean  OEP_InputAbstraction::Register_Button_Pot_Callback(E_POT ePot, E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button_Pot fCallback, void *pUserData)
{
	uint8_t iCallbackIndex;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("Before Register_Button_Pot_Callback:");
	print_callback();	
#endif
	
	// make sure the callback array is allocated
	if ((fCallbacks_Button_Pot == NULL) || (fCallbacks_Button_Pot_UserData == NULL))
		return false;
		
	// sanity check on the input values
	if ((ePot > E_POT_MAX) || (eButton >= E_BUTTON_MAX) || (eButtonAction > E_BUTTON_ACTION_MAX))
		return false;
	
	// having release as button action makes no sense, this can't be used
	if (eButtonAction == E_BUTTON_LONG_PUSH_RELEASE)
		return false;
	
	// compute the callback index we need to address
	iCallbackIndex = CBID_BUTTON_POT_BUILD(eButton, ePot);
	
	// sanity check on the computed index
	if (iCallbackIndex > CBID_BUTTON_POT_MAX)
		return false;
		
	// assign callback
	fCallbacks_Button_Pot[iCallbackIndex] = fCallback;
	fCallbacks_Button_Pot_UserData[iCallbackIndex] = pUserData;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("After Register_Button_Pot_Callback:");
	print_callback();	
#endif
	
	return true;
}

boolean  OEP_InputAbstraction::Register_Switch_Pot_Callback(E_POT ePot, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Switch_Pot fCallback, void *pUserData)
{
	uint8_t iCallbackIndex;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("Before Register_Switch_Pot_Callback:");
	print_callback();	
#endif
	
	// make sure the callback array is allocated
	if ((fCallbacks_Switch_Pot == NULL) || (fCallbacks_Switch_Pot_UserData == NULL))
		return false;

	// sanity check on the input values
	if ((ePot > E_POT_MAX) || (eSwitch >= E_SWITCH_MAX) || (eSwitchState > E_SWITCH_STATE_MAX))
		return false;
	
	// compute the callback index we need to address
	iCallbackIndex = CBID_SWITCH_POT_BUILD(eSwitch, ePot, eSwitchState);
	
	// sanity check on the computed index
	if (iCallbackIndex > CBID_SWITCH_POT_MAX)
		return false;
		
	// assign callback
	fCallbacks_Switch_Pot[iCallbackIndex] = fCallback;
	fCallbacks_Switch_Pot_UserData[iCallbackIndex] = pUserData;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("After Register_Switch_Pot_Callback:");
	print_callback();	
#endif

	return true;
}

boolean  OEP_InputAbstraction::Register_Button_Switch_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Button_Switch fCallback, void *pUserData)
{
	uint8_t iCallbackIndex;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("Before Register_Button_Switch_Callback:");
	print_callback();	
#endif
	
	// make sure the callback array is allocated
	if ((fCallbacks_Button_Switch == NULL) || (fCallbacks_Button_Switch_UserData == NULL))
		return false;
	
	// sanity check on the input values
	if ((eButton >= E_BUTTON_MAX) || (eButtonAction > E_BUTTON_ACTION_MAX) || (eSwitch >= E_SWITCH_MAX) || (eSwitchState > E_SWITCH_STATE_MAX))
		return false;
	
	// compute the callback index we need to address
	iCallbackIndex = CBID_BUTTON_SWITCH_BUILD(eButton, eSwitch, eButtonAction, eSwitchState);
	
	// sanity check on the computed index
	if (iCallbackIndex > CBID_BUTTON_SWITCH_MAX)
		return false;
		
	// assign callback
	fCallbacks_Button_Switch[iCallbackIndex] = fCallback;
	fCallbacks_Button_Switch_UserData[iCallbackIndex] = pUserData;
	
#ifdef LOG_CALLBACK_INFO
	serial_manager.println("After Register_Button_Switch_Callback:");
	print_callback();	
#endif
	
	return true;
}


boolean  OEP_InputAbstraction::Unregister_Button_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button fCallback, void *pUserData)
{
	return Register_Button_Callback(eButton, eButtonAction, NULL, NULL);
}

boolean  OEP_InputAbstraction::Unregister_Button_Pot_Callback(E_POT ePot, E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, fCallback_Button_Pot fCallback, void *pUserData)
{
	return Register_Button_Pot_Callback(ePot, eButton, eButtonAction, NULL, NULL);
}

boolean  OEP_InputAbstraction::Unregister_Switch_Pot_Callback(E_POT ePot, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Switch_Pot fCallback, void *pUserData)
{
	return Register_Switch_Pot_Callback(ePot, eSwitch, eSwitchState, NULL, NULL);
}

boolean  OEP_InputAbstraction::Unregister_Button_Switch_Callback(E_BUTTON eButton, E_BUTTON_ACTION eButtonAction, E_SWITCH eSwitch, E_SWITCH_STATE eSwitchState, fCallback_Button_Switch fCallback, void *pUserData)
{
	return Register_Button_Switch_Callback(eButton, eButtonAction, eSwitch, eSwitchState, NULL, NULL);
}






OEP_Pot::OEP_Pot(OEP_InputAbstraction *pParent, E_POT ePot)
{
	this->pParent = pParent;
	this->ePot = ePot;
	iPin = -1;
	iValue = 0;
	iValuePrevious = 0;
}

OEP_Pot::~OEP_Pot(void)
{
}

int OEP_Pot::setup(void)
{
	// perform initial readout of the pot value
	iValue = AcquireValue();
	iValuePrevious = iValue;
	
	return 0;
}

void OEP_Pot::loop(void)
{
	iValue = AcquireValue();
	if (abs(iValue - iValuePrevious) > pParent->VariationThreshold_Pot)
	{
		iValuePrevious = iValue;
		
		if (EvaluateCombi_ButtonPot() == false)
		{
			if (EvaluateCombi_SwitchPot() == false)
			{
				pParent->PotModified(ePot, iValue);
			}
		}
	}
}


boolean OEP_Pot::EvaluateCombi_ButtonPot(void)
{
	uint8_t iButtonIndex, iCallbackIndex;
	OEP_Button* pButton;
	
	for (iButtonIndex = (uint8_t)E_BUTTON_LEFT; iButtonIndex <= (uint8_t)E_BUTTON_RIGHT; iButtonIndex++)
	{
		pButton = pParent->GetInstance_Button((E_BUTTON)iButtonIndex);
		if (pButton == NULL)
			continue;

		// compute the callback index we need to address
		iCallbackIndex = CBID_BUTTON_POT_BUILD(iButtonIndex, ePot);
		if (iCallbackIndex > CBID_BUTTON_POT_MAX)
			continue;
		
		if ((pParent->fCallbacks_Button_Pot[iCallbackIndex]) &&
			(pButton->eLongPress_State == OEP_Button::E_LP_STATE_PRESS))
		{
			(void)pParent->fCallbacks_Button_Pot[iCallbackIndex](pParent->fCallbacks_Button_Pot_UserData[iCallbackIndex], iValue);
			return true;
		}		
	}

	return false;
}

boolean OEP_Pot::EvaluateCombi_SwitchPot(void)
{
	uint8_t iSwitchIndex, iSwitchStateIndex;
	OEP_Switch* pSwitch;
	
	for (iSwitchIndex = (uint8_t)E_SWITCH_LEFT; iSwitchIndex <= (uint8_t)E_SWITCH_RIGHT; iSwitchIndex++)
	{
		// get the pointer to the switch we're looking at
		pSwitch = pParent->GetInstance_Switch((E_SWITCH)iSwitchIndex);
		if (pSwitch == NULL)
			continue;
		
		for (iSwitchStateIndex = (uint8_t)E_SWITCH_STATE_LEFT; iSwitchStateIndex <= (uint8_t)E_SWITCH_STATE_RIGHT; iSwitchStateIndex++)
		{
			if ((pParent->fCallbacks_Switch_Pot[CBID_SWITCH_POT_BUILD(iSwitchIndex, ePot, iSwitchStateIndex)]) &&
				(pSwitch->eState == (E_SWITCH_STATE)iSwitchStateIndex))
			{
				(void)pParent->fCallbacks_Switch_Pot[CBID_SWITCH_POT_BUILD(iSwitchIndex, ePot, iSwitchStateIndex)](pParent->fCallbacks_Switch_Pot_UserData[CBID_SWITCH_POT_BUILD(iSwitchIndex, ePot, iSwitchStateIndex)], iValue);
				return true;
			}
		}
	}
	
	return false;
}

int OEP_Pot::AcquireValue(void)
{
	int iTemp;
	
	iTemp = 1023 - analogRead(iPin);
	
	// TODO add filtering here
	
	return iTemp;
}

void OEP_Pot::SetPin(int iPin)
{
	this->iPin = iPin;
}






OEP_Switch::OEP_Switch(OEP_InputAbstraction *pParent, E_SWITCH eSwitch, boolean bInverted)
{
	this->pParent = pParent;
	this->eSwitch = eSwitch;
	this->bInverted = bInverted;
	iPin = -1;
	eState = E_SWITCH_STATE_LEFT;
	eStatePrevious = E_SWITCH_STATE_LEFT;
}

OEP_Switch::~OEP_Switch(void)
{
}

int OEP_Switch::setup(void)
{
	// perform initial readout of the pot value
	eState = AcquireState();
	eStatePrevious = eState;
	
	return 0;
}

void OEP_Switch::loop(void)
{
	eState = AcquireState();
		
	if (eState != eStatePrevious)
	{
		eStatePrevious = eState;
		
		if (EvaluateCombi_ButtonSwitch() == false)
			pParent->SwitchModified(eSwitch, eState);
	}
}

E_SWITCH_STATE OEP_Switch::AcquireState(void)
{
	int iSwitchValue;
	E_SWITCH_STATE eTempState;
	
	iSwitchValue = analogRead(iPin);

	// TODO add filtering here

	if (iSwitchValue < 256)
	{
		if (bInverted == false)
			eTempState = E_SWITCH_STATE_LEFT;
		else
			eTempState = E_SWITCH_STATE_RIGHT;
	}
	else if (iSwitchValue > 768)
	{
		if (bInverted == false)
			eTempState = E_SWITCH_STATE_RIGHT;
		else
			eTempState = E_SWITCH_STATE_LEFT;
	}
	else
		eTempState = E_SWITCH_STATE_MIDDLE;
		
	return eTempState;
}

void OEP_Switch::SetPin(int iPin)
{
	this->iPin = iPin;
}

boolean OEP_Switch::EvaluateCombi_ButtonSwitch(void)
{
	uint8_t iButtonIndex;
	OEP_Button* pButton;
	uint8_t iCallbackIndex;
	
	for (iButtonIndex = (uint8_t)E_BUTTON_LEFT; iButtonIndex <= (uint8_t)E_BUTTON_RIGHT; iButtonIndex++)
	{
		// get the pointer to the switch we're looking at
		pButton = pParent->GetInstance_Button((E_BUTTON)iButtonIndex);
		if (pButton == NULL)
			continue;
	
		iCallbackIndex = CBID_BUTTON_SWITCH_BUILD(iButtonIndex, eSwitch, E_BUTTON_LONG_PUSH_PRESSING, eState);
	
		if ((pParent->fCallbacks_Button_Switch[iCallbackIndex]) && (pButton->IsLongPressed() == true))
		{
			(void)pParent->fCallbacks_Button_Switch[iCallbackIndex](pParent->fCallbacks_Button_Switch_UserData[iCallbackIndex]);
			return true;
		}
	}
	
	return false;
}






OEP_Button::OEP_Button(OEP_InputAbstraction *pParent, E_BUTTON eButton)
{
	this->pParent = pParent;
	this->eButton = eButton;
	pBounce = NULL;
	iPin = -1;
	eState = E_BUTTON_OFF;
	eStatePrevious = E_BUTTON_OFF;
	iLongPress_Interval = 5000;
}

OEP_Button::~OEP_Button(void)
{
	if (pBounce != NULL)
		delete ((Bounce*)pBounce);
}

int OEP_Button::setup(void)
{
	int iRet = 0;
	
	// create button stuff
	if (iPin == -1)
	{
		iRet = -1;
	}
	else
	{
		pBounce = new Bounce(iPin, 30 );
		if (pBounce == NULL)
			iRet = -1;
		pinMode(iPin, INPUT);
	}
	for (uint8_t iIndex = 0; iIndex < 50; iIndex++)
	{
		((Bounce*)pBounce)->update();
		delay(1);
	}
	
	// perform initial readout of the pot value
	eState = AcquireState();
	eStatePrevious = eState;
	eLongPress_State = E_LP_STATE_OFF;
	
	return iRet;
}

void OEP_Button::loop(void)
{
	eState = AcquireState();
		
	if (eState != eStatePrevious)
	{
		// when going from Off to On, change the latch state
		if (eState == E_BUTTON_ON)
		{
			if (eLatchState == E_BUTTON_OFF)
			{
				eLatchState = E_BUTTON_ON;
			}
			else
			{
				eLatchState = E_BUTTON_OFF;
			}
			pParent->ButtonLatchModified(eButton, eLatchState);
			eLongPress_State = E_LP_STATE_EVALUATE;
			iLongPress_TimeRef = OEP_InputAbstraction_timeElapsed;
		}
		else
		{
			if (eLongPress_State == E_LP_STATE_PRESS)
			{
				if (pParent->fCallbacks_Button[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_RELEASE)])
				{
					(void)pParent->fCallbacks_Button[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_RELEASE)](pParent->fCallbacks_Button_UserData[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_RELEASE)]);
				}
			}
			eLongPress_State = E_LP_STATE_OFF;
		}
		eStatePrevious = eState;
		pParent->ButtonModified(eButton, eState);
	}
	
	if (eState == E_BUTTON_ON)
	{
		if ((eLongPress_State == E_LP_STATE_EVALUATE) &&
			(OEP_InputAbstraction_timeElapsed - iLongPress_TimeRef >= iLongPress_Interval))
		{
			eLongPress_State = E_LP_STATE_PRESS;
			if (pParent->fCallbacks_Button[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_PRESSING)])
			{
				(void)pParent->fCallbacks_Button[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_PRESSING)](pParent->fCallbacks_Button_UserData[CBID_BUTTON_BUILD(eButton, E_BUTTON_LONG_PUSH_PRESSING)]);
			}
		}
	}
}

E_BUTTON_STATE OEP_Button::AcquireState(void)
{
	E_BUTTON_STATE eTempState = E_BUTTON_OFF;
	
	if (pBounce != NULL)
	{
		((Bounce*)pBounce)->update();
		if (((Bounce*)pBounce)->read() == HIGH)
			eTempState = E_BUTTON_OFF;
		else
			eTempState = E_BUTTON_ON;
	}
		
	return eTempState;
}

void OEP_Button::SetPin(int iPin)
{
	this->iPin = iPin;
}

void OEP_Button::SetLongPressTime(uint32_t iTimeInMs)
{
	iLongPress_Interval = iTimeInMs;
}



