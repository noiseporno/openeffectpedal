/* OpenEffectPedal Applicative helper library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_APP_ExtLevels__h__
#define __OEP_APP_ExtLevels__h__


#include "Arduino.h"
#include "OpenEffectPedal.h"	// for the OEP_Abstraction
#include <EEPROM.h>

// storage size required by this module:
//  4 bytes values
//  8 bytes reserved for future use
#define OEP_APP_EXTLEVELS_STORAGE_SIZE_IN_BYTES				(sizeof(OEP_APP_ExtLevels::T_STORAGE))

class OEP_APP_ExtLevels
{
public:

  enum E_UI_MODE
  {
	  E_UI_MODE_OFF = 0,
	  E_UI_MODE_VOL,
	  E_UI_MODE_EQ,
	  E_UI_MODE_AGC,
	  E_UI_MODE_VOL_EQ,
	  E_UI_MODE_VOL_AGC,
	  E_UI_MODE_EQ_AGC,
	  E_UI_MODE_VOL_EQ_AGC,
  };
  
  enum E_SAVE_MODE
  {
	  E_SAVE_MODE_DISABLED = 0,
	  E_SAVE_MODE_ENABLED,
  };
  
  struct T_STORAGE
  {
	  uint8_t iValue1;
	  uint8_t iValue2;
	  uint8_t iValue3;
	  uint8_t iValue4;
	  
	  uint8_t iReserved0;
	  uint8_t iReserved1;
	  uint8_t iReserved2;
	  uint8_t iReserved3;
	  uint8_t iReserved4;
	  uint8_t iReserved5;
	  uint8_t iReserved6;
	  uint8_t iReserved7;
	  
  };
  
  OEP_APP_ExtLevels(class OEP_Abstraction *pOEP, E_BUTTON eButton, E_UI_MODE eUiMode, E_SAVE_MODE eSaveMode, int iEepromAddress)
  {
	bInitialized = false;
    this->pOEP = pOEP;
	this->eButton = eButton;
	this->eUiMode = eUiMode;
	this->eSaveMode = eSaveMode;
	this->iEepromAddress = iEepromAddress;
  };
	
  ~OEP_APP_ExtLevels(void)
  {
	  
  };

  int  setup(void);
  void loop(void);
  

private:
  enum E_SETTING_MODE
  {
	E_SETTING_MODE_NONE,
	E_SETTING_MODE_INPUT,
	E_SETTING_MODE_OUTPUT,
  };
  
  E_SETTING_MODE     eSettingMode;
  E_UI_MODE 		 eUiMode;
  E_SAVE_MODE 		 eSaveMode;
  E_BUTTON			 eButton;
  int				 iEepromAddress;

  boolean			 bInitialized;
  
  OEP_Abstraction   *pOEP;
  uint8_t			*pDisplayBuffer;
  
  int  RegisterCallbacks(void);
  void UnregisterCallbacks(void);
  void UpdateDisplay(void);
  void UpdateDisplay_Levels(void);
  void UpdateDisplay_EQ(void);
  void UpdateDisplay_AVC(void);
  void StoreData(void);
  void RestoreData(void);

  static int ButtonPressed(void *pUserData);
  static int ButtonReleased(void *pUserData);
  static int ButtonPotS1Left(void *pUserData, int iNewPotValue);
  static int ButtonPotS1Right(void *pUserData, int iNewPotValue);
  static int ButtonPotS2Left(void *pUserData, int iNewPotValue);
  static int ButtonPotS2Right(void *pUserData, int iNewPotValue);
  static int ButtonSrcSwitchLeft(void *pUserData);
  static int ButtonSrcSwitchMiddle(void *pUserData);
  static int ButtonSrcSwitchRight(void *pUserData);
  static int ButtonVolSwitchLeft(void *pUserData);
  static int ButtonVolSwitchMiddle(void *pUserData);
  static int ButtonVolSwitchRight(void *pUserData);
};

#endif
