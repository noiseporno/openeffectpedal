/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_Abstraction__h__
#define __OEP_Abstraction__h__


#include "Arduino.h"
#include "OEP_Defines.h"
#include "OEP_InputAbstraction.h"
#include "OEP_OutputAbstraction.h"
#include "OEP_AudioAbstraction.h"
#include "OEP_StorageAbstraction.h"

class OEP_Abstraction : public OEP_InputAbstraction, public OEP_OutputAbstraction, public OEP_AudioAbstraction, public OEP_StorageAbstraction
{
public:
  int iSetupRet_InputAbstraction;
  int iSetupRet_OutputAbstraction;
  int iSetupRet_AudioAbstraction;
  int iSetupRet_StorageAbstaction;

  OEP_Abstraction(E_MB_Type eMbType, E_EXT_Type eExtType);
  ~OEP_Abstraction(void);

  int  setup(void);
  void loop(void);
  
private:
  E_MB_Type 		eMotherboardType;
  E_EXT_Type		eExtensionType;

};


#endif
