/* OpenEffectPedal Applicative helper library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "Arduino.h"
#include "OEP_APP_MixIO.h"
#include <util/crc16.h>
#include <eeprom.h>

#define OEP_APP_MAXIO_LOG

#ifdef OEP_APP_MAXIO_LOG
#include <RBD_SerialManager.h>
extern RBD::SerialManager 			serial_manager;
#endif

static float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int OEP_APP_MixIO::setup(void)
{
	int rc;
	
	// make sure we can proceed 
	if ((pOEP == NULL) || (pMix_1to2 == NULL) || (pMix_2to1 == NULL))
	{
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO]  Invalid pointers");
#endif
		return -1;
	}

	// adapt pot assignment from LinkMode
	switch(eLinkMode)
	{
		default:
		case E_LINK_MODE_P1_P2_P3_P4:
		case E_LINK_MODE_P1_P2:
			ePotS1L = E_POT_1;
			ePotS2L = E_POT_2;
			ePotS1R = E_POT_3;
			ePotS2R = E_POT_4;
			break;
		case E_LINK_MODE_P1_P3_P2_P4:
			ePotS1L = E_POT_1;
			ePotS2L = E_POT_3;
			ePotS1R = E_POT_2;
			ePotS2R = E_POT_4;
			break;
		case E_LINK_MODE_P1_P4:
			ePotS1L = E_POT_1;
			ePotS2L = E_POT_4;
			ePotS1R = E_POT_2;
			ePotS2R = E_POT_3;
			break;

	}
	
	// register the callbacks
	rc = RegisterCallbacks();
	if (rc != 0)
	{
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.print("[OEP_APP_MixIO] Failed to register callbacks: ");
		serial_manager.println(rc);
#endif
		return -2;
	}
	
	// loadi gains and default 
	RestoreData();
		
	bInitialized = true;
	return 0;
}

void OEP_APP_MixIO::loop(void)
{
}

void OEP_APP_MixIO::select_mix_mode(E_MIX_MODE eMixMode)
{
	// only set mode when button is not pushed
	if (eMixMode == E_MIX_MODE_STREAM1)
	{
		pMix_2to1->gain(0, 1.0);
		pMix_2to1->gain(1, 1.0);
		pMix_2to1->gain(2, 0.0);
		pMix_2to1->gain(3, 0.0);
		pMix_1to2->gain(0, 1.0);
		pMix_1to2->gain(1, 1.0);
		pMix_1to2->gain(2, 0.0);
		pMix_1to2->gain(3, 0.0);
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO] pMix_NtoN to in1");
#endif
	}
	else if (eMixMode == E_MIX_MODE_STREAM2)
	{
		pMix_2to1->gain(0, 0.0);
		pMix_2to1->gain(1, 0.0);
		pMix_2to1->gain(2, 1.0);
		pMix_2to1->gain(3, 1.0);
		pMix_1to2->gain(0, 0.0);
		pMix_1to2->gain(1, 0.0);
		pMix_1to2->gain(2, 1.0);
		pMix_1to2->gain(3, 1.0);
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO] pMix_NtoN to in2");
#endif
	}
	else
	{
		pMix_2to1->gain(0, fGainS1InL);
		pMix_2to1->gain(1, fGainS1InR);
		pMix_2to1->gain(2, fGainS2InL);
		pMix_2to1->gain(3, fGainS2InR);
		pMix_1to2->gain(0, fGainS1OutL);
		pMix_1to2->gain(1, fGainS1OutR);
		pMix_1to2->gain(2, fGainS2OutL);
		pMix_1to2->gain(3, fGainS2OutR);
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO] pMix_NtoN to mix");
#endif
	}
	this->eMixMode = eMixMode;
	pMix_1to2->print_gain();
	pMix_2to1->print_gain();
}

void OEP_APP_MixIO::set_input_gain(float fGainS1Left, float fGainS2Left, float fGainS1Right, float fGainS2Right)
{
	fGainS1InL = fGainS1Left;
	fGainS1InR = fGainS1Right;
	fGainS2InL = fGainS2Left;
	fGainS2InR = fGainS2Right;
	// update the mixers
	select_mix_mode(eMixMode);
}

void OEP_APP_MixIO::set_output_gain(float fGainS1Left, float fGainS2Left, float fGainS1Right, float fGainS2Right)
{
	fGainS1OutL = fGainS1Left;
	fGainS1OutR = fGainS1Right;
	fGainS2OutL = fGainS2Left;
	fGainS2OutR = fGainS2Right;
	// update the mixers
	select_mix_mode(eMixMode);
}

int OEP_APP_MixIO::RegisterCallbacks(void)
{
	
	if (pOEP->Register_Button_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPressed, this) == false)
		return -2;
	if (pOEP->Register_Button_Callback(eButton, E_BUTTON_LONG_PUSH_RELEASE, OEP_APP_MixIO::ButtonReleased, this) == false)
		return -3;
	if ((eUiMode == E_UI_MODE_VOL) ||(eUiMode == E_UI_MODE_VOL_SRC))
	{
		if (pOEP->Register_Button_Pot_Callback(ePotS1L, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS1Left, this) == false)
			return -4;
		if (pOEP->Register_Button_Pot_Callback(ePotS2L, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS2Left, this) == false)
			return -5;
		if (pOEP->Register_Button_Pot_Callback(ePotS1R, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS1Right, this) == false)
			return -6;
		if (pOEP->Register_Button_Pot_Callback(ePotS2R, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS2Right, this) == false)
			return -7;
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_LEFT, OEP_APP_MixIO::ButtonVolSwitchLeft, this) == false)
			return -8;
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_MIDDLE, OEP_APP_MixIO::ButtonVolSwitchMiddle, this) == false)
			return -9;
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_RIGHT, OEP_APP_MixIO::ButtonVolSwitchRight, this) == false)
			return -10;
	}
	if ((eUiMode == E_UI_MODE_SRC) || (eUiMode == E_UI_MODE_VOL_SRC))
	{
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_LEFT, OEP_APP_MixIO::ButtonSrcSwitchLeft, this) == false)
			return -11;
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_MIDDLE, OEP_APP_MixIO::ButtonSrcSwitchMiddle, this) == false)
			return -12;
		if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_RIGHT, OEP_APP_MixIO::ButtonSrcSwitchRight, this) == false)
			return -13;
	}
	
	return 0;
}

void OEP_APP_MixIO::UnregisterCallbacks(void)
{
	pOEP->Unregister_Button_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPressed, this);
	pOEP->Unregister_Button_Callback(eButton, E_BUTTON_LONG_PUSH_RELEASE, OEP_APP_MixIO::ButtonReleased, this);
	if ((eUiMode == E_UI_MODE_VOL) ||(eUiMode == E_UI_MODE_VOL_SRC))
	{
		pOEP->Unregister_Button_Pot_Callback(ePotS1L, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS1Left, this);
		pOEP->Unregister_Button_Pot_Callback(ePotS2L, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS2Left, this);
		pOEP->Unregister_Button_Pot_Callback(ePotS1R, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS1Right, this);
		pOEP->Unregister_Button_Pot_Callback(ePotS2R, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_MixIO::ButtonPotS2Right, this);
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_LEFT, OEP_APP_MixIO::ButtonVolSwitchLeft, this);
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_MIDDLE, OEP_APP_MixIO::ButtonVolSwitchMiddle, this);
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_RIGHT, OEP_APP_MixIO::ButtonVolSwitchRight, this);
	}
	if ((eUiMode == E_UI_MODE_SRC) || (eUiMode == E_UI_MODE_VOL_SRC))
	{
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_LEFT, OEP_APP_MixIO::ButtonSrcSwitchLeft, this);
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_MIDDLE, OEP_APP_MixIO::ButtonSrcSwitchMiddle, this);
		pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_RIGHT, OEP_APP_MixIO::ButtonSrcSwitchRight, this);
	}
}

int OEP_APP_MixIO::ButtonPressed(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;
	uint32_t iBufferSize;

#ifdef OEP_APP_MAXIO_LOG
	serial_manager.println("[OEP_APP_MixIO] LongPress MixIO");
#endif
	
	// store the screen
	iBufferSize = pOEP_MixIO->pOEP->pDisplay->getBufferSize();
	pOEP_MixIO->pDisplayBuffer = new uint8_t [iBufferSize];
	if (pOEP_MixIO->pDisplayBuffer != NULL)
	{
		pOEP_MixIO->pOEP->pDisplay->storeBuffer(pOEP_MixIO->pDisplayBuffer);
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO] Stored display buffer");
#endif
		pOEP_MixIO->pOEP->pDisplay->clearDisplay();
		pOEP_MixIO->pOEP->pDisplay->display();
	}
	
	// evaluate switches value to preset the right mode
	pOEP_MixIO->UpdateSrcMode(pOEP_MixIO->pOEP->SwitchLeft_State());
	pOEP_MixIO->UpdateVolMode(pOEP_MixIO->pOEP->SwitchRight_State());

	return 0;
}

int OEP_APP_MixIO::ButtonReleased(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

#ifdef OEP_APP_MAXIO_LOG
	serial_manager.println("[OEP_APP_MixIO] LongRelease MixIO");
#endif
	
	// save gains and default 
	pOEP_MixIO->StoreData();
	
	// restore the screen
	if (pOEP_MixIO->pDisplayBuffer != NULL)
	{
		pOEP_MixIO->pOEP->pDisplay->restoreBuffer(pOEP_MixIO->pDisplayBuffer);
		pOEP_MixIO->pOEP->pDisplay->display();
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.println("[OEP_APP_MixIO] Restored display buffer");
#endif
		delete[] pOEP_MixIO->pDisplayBuffer;
	}
	
	return 0;
}

int OEP_APP_MixIO::ButtonPotS1Left(void *pUserData, int iNewPotValue)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);
	
#ifdef OEP_APP_MAXIO_LOG
	serial_manager.print("[OEP_APP_MixIO] PotS1Left = ");
	serial_manager.println(fGain);
#endif
	
	if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
	{
		pOEP_MixIO->fGainS1InL = fGain;
	}
	else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
	{
		pOEP_MixIO->fGainS1OutL = fGain;
	}

	// if we're in linked mode, adapt the right pot as well
	if ((pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P2) || (pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P4))
	{
		if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
		{
			pOEP_MixIO->fGainS1InR = fGain;
		}
		else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
		{
			pOEP_MixIO->fGainS1OutR = fGain;
		}
	}

	// apply the changes if needed
	if (pOEP_MixIO->eMixMode == E_MIX_MODE_MIX)
		pOEP_MixIO->select_mix_mode(pOEP_MixIO->eMixMode);
	
	pOEP_MixIO->UpdateDisplay();

	return 0;
}

int OEP_APP_MixIO::ButtonPotS1Right(void *pUserData, int iNewPotValue)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);
	
	// if we're in individual mode, adapt the right value
	if ((pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P2_P3_P4) || (pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P3_P2_P4))
	{
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.print("[OEP_APP_MixIO] PotS1Right = ");
		serial_manager.println(fGain);
#endif
		
		if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
		{
			pOEP_MixIO->fGainS1InR = fGain;
		}
		else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
		{
			pOEP_MixIO->fGainS1OutR = fGain;
		}

		// apply the changes if needed
		if (pOEP_MixIO->eMixMode == E_MIX_MODE_MIX)
			pOEP_MixIO->select_mix_mode(pOEP_MixIO->eMixMode);	

		pOEP_MixIO->UpdateDisplay();
	}

	return 0;
}

int OEP_APP_MixIO::ButtonPotS2Left(void *pUserData, int iNewPotValue)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);

#ifdef OEP_APP_MAXIO_LOG
	serial_manager.print("[OEP_APP_MixIO] PotS2Left = ");
	serial_manager.println(fGain);
#endif
	
	if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
	{
		pOEP_MixIO->fGainS2InL = fGain;
	}
	else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
	{
		pOEP_MixIO->fGainS2OutL = fGain;
	}

	// if we're in linked mode, adapt the right pot as well
	if ((pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P2) || (pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P4))
	{
		if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
		{
			pOEP_MixIO->fGainS2InR = fGain;
		}
		else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
		{
			pOEP_MixIO->fGainS2OutR = fGain;
		}
	}
	
	// apply the changes if needed
	if (pOEP_MixIO->eMixMode == E_MIX_MODE_MIX)
		pOEP_MixIO->select_mix_mode(pOEP_MixIO->eMixMode);
	
	pOEP_MixIO->UpdateDisplay();

	return 0;
}

int OEP_APP_MixIO::ButtonPotS2Right(void *pUserData, int iNewPotValue)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);

	// if we're in individual mode, adapt the right value
	if ((pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P2_P3_P4) || (pOEP_MixIO->eLinkMode == E_LINK_MODE_P1_P3_P2_P4))
	{
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.print("[OEP_APP_MixIO] PotS2Right = ");
		serial_manager.println(fGain);
#endif
		
		if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_INPUT)
		{
			pOEP_MixIO->fGainS2InR = fGain;
		}
		else if (pOEP_MixIO->eSettingMode == E_SETTING_MODE_OUTPUT)
		{
			pOEP_MixIO->fGainS2OutR = fGain;
		}

		// apply the changes if needed
		if (pOEP_MixIO->eMixMode == E_MIX_MODE_MIX)
			pOEP_MixIO->select_mix_mode(pOEP_MixIO->eMixMode);
	
		pOEP_MixIO->UpdateDisplay();
	}

	return 0;
}

int OEP_APP_MixIO::ButtonSrcSwitchLeft(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateSrcMode(E_SWITCH_STATE_LEFT);
	
	return 0;
}

int OEP_APP_MixIO::ButtonSrcSwitchMiddle(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateSrcMode(E_SWITCH_STATE_MIDDLE);
	
	return 0;
}

int OEP_APP_MixIO::ButtonSrcSwitchRight(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateSrcMode(E_SWITCH_STATE_RIGHT);
	
	return 0;
}

int OEP_APP_MixIO::ButtonVolSwitchLeft(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateVolMode(E_SWITCH_STATE_LEFT);
	
	return 0;
}

int OEP_APP_MixIO::ButtonVolSwitchMiddle(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateVolMode(E_SWITCH_STATE_MIDDLE);
	
	return 0;
}

int OEP_APP_MixIO::ButtonVolSwitchRight(void *pUserData)
{
	OEP_APP_MixIO *pOEP_MixIO = (OEP_APP_MixIO *)pUserData;

	pOEP_MixIO->UpdateVolMode(E_SWITCH_STATE_RIGHT);
	
	return 0;
}

void OEP_APP_MixIO::UpdateSrcMode(E_SWITCH_STATE eNewState)
{
	// adapt the mix mode
	switch (eNewState)
	{
		default:
		case E_SWITCH_STATE_LEFT:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Listen to stream 1");
#endif
			eMixMode = E_MIX_MODE_STREAM1;
			break;
		case E_SWITCH_STATE_MIDDLE:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Listen to mix");
#endif
			eMixMode = E_MIX_MODE_MIX;
			break;
		case E_SWITCH_STATE_RIGHT:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Listen to stream 2");
#endif
			eMixMode = E_MIX_MODE_STREAM2;
			break;
	}
	
	// select the mix mode
	select_mix_mode(eMixMode);
	UpdateDisplay();
}

void OEP_APP_MixIO::UpdateVolMode(E_SWITCH_STATE eNewState)
{
	// adapt the mix mode
	switch (eNewState)
	{
		case E_SWITCH_STATE_LEFT:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Setting input");
#endif
			eSettingMode = E_SETTING_MODE_INPUT;
			break;
		case E_SWITCH_STATE_MIDDLE:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Setting none");
#endif
			eSettingMode = E_SETTING_MODE_NONE;
			break;
		case E_SWITCH_STATE_RIGHT:
#ifdef OEP_APP_MAXIO_LOG
			serial_manager.println("[OEP_APP_MixIO] Setting output");
#endif
			eSettingMode = E_SETTING_MODE_OUTPUT;
			break;
	}
	UpdateDisplay();
}

void OEP_APP_MixIO::UpdateDisplay(void)
{
	pOEP->pDisplay->setTextSize(1);
	pOEP->pDisplay->setTextColor(WHITE);
	pOEP->pDisplay->setCursor(10,0);
	pOEP->pDisplay->clearDisplay();

	pOEP->pDisplay->print("Src: ");
	switch (eMixMode)
	{
		default:
		case E_MIX_MODE_STREAM1:
			pOEP->pDisplay->print("In1");
			break;
		case E_MIX_MODE_MIX:
			pOEP->pDisplay->print("Mix");
			break;
		case E_MIX_MODE_STREAM2:
			pOEP->pDisplay->print("In2");
			break;
	}

	pOEP->pDisplay->print("  Set: ");
	switch (eSettingMode)
	{
		default:
		case E_SETTING_MODE_INPUT:
			pOEP->pDisplay->print("In");
			break;
		case E_SETTING_MODE_NONE:
			pOEP->pDisplay->print("-");
			break;
		case E_SETTING_MODE_OUTPUT:
			pOEP->pDisplay->print("Out");
			break;
	}

	if (eSettingMode == E_SETTING_MODE_INPUT)
	{
		pOEP->pDisplay->setCursor(10,10);
		pOEP->pDisplay->print("In1 : ");
		pOEP->pDisplay->print(fGainS1InL, 2);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS1InR, 2);
		pOEP->pDisplay->setCursor(10,20);
		pOEP->pDisplay->print("In2 : ");
		pOEP->pDisplay->print(fGainS2InL, 2);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS2InR, 2);
	}
	
	if (eSettingMode == E_SETTING_MODE_OUTPUT)
	{
		pOEP->pDisplay->setCursor(10,10);
		pOEP->pDisplay->print("Out1 : ");
		pOEP->pDisplay->print(fGainS1OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS1OutR, 1);
		pOEP->pDisplay->setCursor(10,20);
		pOEP->pDisplay->print("Out2 : ");
		pOEP->pDisplay->print(fGainS2OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS2OutR, 1);
	}
	
	pOEP->pDisplay->display();
	
}

#define STORAGE_MAGIC			0x5A01

void OEP_APP_MixIO::StoreData(void)
{
	T_STORAGE tData = {0};
	
	// prepare the data
	tData.iGainS1InL = (uint8_t)(fGainS1InL * 100);
	tData.iGainS1InR = (uint8_t)(fGainS1InR * 100);
	tData.iGainS2InL = (uint8_t)(fGainS2InL * 100);
	tData.iGainS2InR = (uint8_t)(fGainS2InR * 100);
	tData.iGainS1OutL = (uint8_t)(fGainS1OutL * 100);
	tData.iGainS1OutR = (uint8_t)(fGainS1OutR * 100);
	tData.iGainS2OutL = (uint8_t)(fGainS2OutL * 100);
	tData.iGainS2OutR = (uint8_t)(fGainS2OutR * 100);
	tData.iMixMode = (uint8_t)eMixMode;

	// write the data
	//memcpy(&aData[0], &tData, OEP_APP_MIXIO_STORAGE_SIZE_IN_BYTES);
	pOEP->Store( STORAGE_MAGIC, iEepromAddress, tData );

#ifdef OEP_APP_MAXIO_LOG
	serial_manager.println("[OEP_APP_MixIO] Configuration saved to eeprom");
#endif
}

void OEP_APP_MixIO::RestoreData(void)
{
	T_STORAGE tData = {0};
	int rc;
	
	// read the data
	rc = pOEP->Restore( STORAGE_MAGIC, iEepromAddress, tData);
	if (rc != 0)
	{
#ifdef OEP_APP_MAXIO_LOG
		serial_manager.print("[OEP_APP_MixIO] Failed to reload config from eeprom : ");
		serial_manager.println(rc);
#endif
		return;
	}	
	
	// decode the data
	fGainS1InL = ((float)tData.iGainS1InL) / 100;
	fGainS1InR = ((float)tData.iGainS1InR) / 100;
	fGainS2InL = ((float)tData.iGainS2InL) / 100;
	fGainS2InR = ((float)tData.iGainS2InR) / 100;
	fGainS1OutL = ((float)tData.iGainS1OutL) / 100;
	fGainS1OutR = ((float)tData.iGainS1OutR) / 100;
	fGainS2OutL = ((float)tData.iGainS2OutL) / 100;
	fGainS2OutR = ((float)tData.iGainS2OutR) / 100;
	eMixMode = (E_MIX_MODE)tData.iMixMode;

#ifdef OEP_APP_MAXIO_LOG
	serial_manager.println("[OEP_APP_MixIO] Configuration reloaded from eeprom");
#endif
}
