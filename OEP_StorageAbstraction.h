/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_StorageAbstraction__h__
#define __OEP_StorageAbstraction__h__


#include "Arduino.h"
#include "OEP_Defines.h"
#include <util/crc16.h>
#include <eeprom.h>


class OEP_StorageAbstraction
{
public:

	/*********************************
	*             Defines            *
	*********************************/
	#define STORAGE_OFFSET_MAGIC	0
	#define STORAGE_OFFSET_CRC		2
	#define STORAGE_OFFSET_SIZE		4
	#define STORAGE_OFFSET_DATA		8

	
	/*********************************
	*              Types             *
	*********************************/
	
	/*********************************
	*           Variables            *
	*********************************/

  
  
	/*********************************
	*         Main functions         *
	*********************************/
	OEP_StorageAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType)
	{
		// we could do here a check on the platform and select the right external eeprom
	};
	
	~OEP_StorageAbstraction(void)
	{
		
	};


    
	/*********************************
	*         Storage functions      *
	*********************************/
	template< typename T > int Store( uint16_t iMagic, uint32_t iAddress, T &t )
	{
		uint8_t aHeader[STORAGE_OFFSET_DATA];
		uint32_t iIndex;
		uint16_t iCrc16;
		uint32_t iDataSize = sizeof(T);
		uint8_t *pData = (uint8_t *)&t;

		// sanity check on address & sizes
		if (iAddress >= EEPROM.length())
			return -1;
		if (iAddress + iDataSize > EEPROM.length())
			return -2;
		
		// prepare the header
		iCrc16 = 0;
		for (iIndex = 0; iIndex < iDataSize; iIndex++)
			iCrc16 = _crc_ccitt_update(iCrc16, pData[iIndex]);
		aHeader[STORAGE_OFFSET_MAGIC + 0] = iMagic & 0xFF;
		aHeader[STORAGE_OFFSET_MAGIC + 1] = (iMagic >> 8) & 0xFF;
		aHeader[STORAGE_OFFSET_CRC   + 0] = iCrc16 & 0xFF;
		aHeader[STORAGE_OFFSET_CRC   + 1] = (iCrc16 >> 8) & 0xFF;
		aHeader[STORAGE_OFFSET_SIZE  + 0] = iDataSize & 0xFF;
		aHeader[STORAGE_OFFSET_SIZE  + 1] = (iDataSize >> 8) & 0xFF;
		aHeader[STORAGE_OFFSET_SIZE  + 2] = (iDataSize >> 16) & 0xFF;
		aHeader[STORAGE_OFFSET_SIZE  + 3] = (iDataSize >> 24) & 0xFF;

		// store the header
		EEPROM.put( iAddress, aHeader );
		
		// store the data
		EEPROM.put( iAddress + STORAGE_OFFSET_DATA, t );

		return 0;
	};

	template< typename T > int Restore( uint16_t iMagic, uint32_t iAddress, T &t )
	{
		uint8_t aHeader[STORAGE_OFFSET_DATA];
		uint32_t iIndex;
		uint16_t iCrc16, iTemp16;
		uint32_t iDataSize = sizeof(T), iTemp32;
		uint8_t *pData = (uint8_t *)&t;

		// sanity check on address & sizes
		if (iAddress >= EEPROM.length())
			return -1;
		if (iAddress + iDataSize > EEPROM.length())
			return -2;

		// read the header
		EEPROM.get( iAddress, aHeader );
		
		// verify the magic
		iTemp16 = ((uint16_t)aHeader[STORAGE_OFFSET_MAGIC + 1] << 8) + aHeader[STORAGE_OFFSET_MAGIC + 0];
		if (iTemp16 != iMagic)
		{
			return -3;
		}
		
		// verify the sizes
		iTemp32 = ((uint32_t)aHeader[STORAGE_OFFSET_SIZE + 3] << 24) +
				  ((uint32_t)aHeader[STORAGE_OFFSET_SIZE + 1] << 16) + 
				  ((uint32_t)aHeader[STORAGE_OFFSET_SIZE + 1] <<  8) + 
				  ((uint32_t)aHeader[STORAGE_OFFSET_SIZE + 0]);
		if (iTemp32 != iDataSize)
		{
			return -4;
		}
		
		// read the data
		EEPROM.get( iAddress + STORAGE_OFFSET_DATA, t );

		// verify the data integrity
		iCrc16 = 0;
		for (iIndex = 0; iIndex < iDataSize; iIndex++)
			iCrc16 = _crc_ccitt_update(iCrc16, pData[iIndex]);
		iTemp16 = ((uint16_t)aHeader[STORAGE_OFFSET_CRC + 1] << 8) + aHeader[STORAGE_OFFSET_CRC + 0];
		if (iTemp16 != iCrc16)
		{
			return -5;
		}

		return 0;
	};
	
	
	// storage uses CRC16 to validate the integrity of the data
	// the magic number is stored along with the data in eeprom
	// this allows to make sure that a record belongs to the right owner
	// it is the application's responsability to use different magic numbers
	// if the same number is used, the functionnality is "disabled"
	
	
	
private:

};


#endif
