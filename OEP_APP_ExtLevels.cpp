/* OpenEffectPedal Applicative helper library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "Arduino.h"
#include "OEP_APP_ExtLevels.h"
#include <util/crc16.h>
#include <eeprom.h>

#define OEP_APP_EXTLEVELS_LOG

#ifdef OEP_APP_EXTLEVELS_LOG
#include <RBD_SerialManager.h>
extern RBD::SerialManager 			serial_manager;
#endif

static float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


int OEP_APP_ExtLevels::setup(void)
{
	int rc;
	
	// make sure we can proceed 
	if (pOEP == NULL)
	{
#ifdef OEP_APP_EXTLEVELS_LOG
		serial_manager.println("[OEP_APP_ExtLevels]  Invalid pointers");
#endif
		return -1;
	}

	// register the callbacks
	rc = RegisterCallbacks();
	if (rc != 0)
	{
#ifdef OEP_APP_EXTLEVELS_LOG
		serial_manager.print("[OEP_APP_ExtLevels] Failed to register callbacks: ");
		serial_manager.println(rc);
#endif
		return -2;
	}
	
	// loadi gains and default 
	RestoreData();
		
	bInitialized = true;
	return 0;
}

void OEP_APP_ExtLevels::loop(void)
{
}

int OEP_APP_ExtLevels::RegisterCallbacks(void)
{
	
	if (pOEP->Register_Button_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPressed, this) == false)
		return -2;
	if (pOEP->Register_Button_Callback(eButton, E_BUTTON_LONG_PUSH_RELEASE, OEP_APP_ExtLevels::ButtonReleased, this) == false)
		return -3;
	if (pOEP->Register_Button_Pot_Callback(E_POT_1, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS1Left, this) == false)
		return -4;
	if (pOEP->Register_Button_Pot_Callback(E_POT_2, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS2Left, this) == false)
		return -5;
	if (pOEP->Register_Button_Pot_Callback(E_POT_3, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS1Right, this) == false)
		return -6;
	if (pOEP->Register_Button_Pot_Callback(E_POT_4, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS2Right, this) == false)
		return -7;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_LEFT, OEP_APP_ExtLevels::ButtonVolSwitchLeft, this) == false)
		return -8;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_MIDDLE, OEP_APP_ExtLevels::ButtonVolSwitchMiddle, this) == false)
		return -9;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_RIGHT, OEP_APP_ExtLevels::ButtonVolSwitchRight, this) == false)
		return -10;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_LEFT, OEP_APP_ExtLevels::ButtonSrcSwitchLeft, this) == false)
		return -11;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_MIDDLE, OEP_APP_ExtLevels::ButtonSrcSwitchMiddle, this) == false)
		return -12;
	if (pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_RIGHT, OEP_APP_ExtLevels::ButtonSrcSwitchRight, this) == false)
		return -13;
	
	return 0;
}

void OEP_APP_ExtLevels::UnregisterCallbacks(void)
{
	pOEP->Unregister_Button_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPressed, this);
	pOEP->Unregister_Button_Callback(eButton, E_BUTTON_LONG_PUSH_RELEASE, OEP_APP_ExtLevels::ButtonReleased, this);
	pOEP->Unregister_Button_Pot_Callback(E_POT_1, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS1Left, this);
	pOEP->Unregister_Button_Pot_Callback(E_POT_2, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS2Left, this);
	pOEP->Unregister_Button_Pot_Callback(E_POT_3, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS1Right, this);
	pOEP->Unregister_Button_Pot_Callback(E_POT_4, eButton, E_BUTTON_LONG_PUSH_PRESSING, OEP_APP_ExtLevels::ButtonPotS2Right, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_LEFT, OEP_APP_ExtLevels::ButtonVolSwitchLeft, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_MIDDLE, OEP_APP_ExtLevels::ButtonVolSwitchMiddle, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_RIGHT, E_SWITCH_STATE_RIGHT, OEP_APP_ExtLevels::ButtonVolSwitchRight, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_LEFT, OEP_APP_ExtLevels::ButtonSrcSwitchLeft, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_MIDDLE, OEP_APP_ExtLevels::ButtonSrcSwitchMiddle, this);
	pOEP->Register_Button_Switch_Callback(eButton, E_BUTTON_LONG_PUSH_PRESSING, E_SWITCH_LEFT, E_SWITCH_STATE_RIGHT, OEP_APP_ExtLevels::ButtonSrcSwitchRight, this);
}

int OEP_APP_ExtLevels::ButtonPressed(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;
	uint32_t iBufferSize;

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.println("[OEP_APP_ExtLevels] LongPress MixIO");
#endif
	
	// store the screen
	iBufferSize = pOEP_ExtLevels->pOEP->pDisplay->getBufferSize();
	pOEP_ExtLevels->pDisplayBuffer = new uint8_t [iBufferSize];
	if (pOEP_ExtLevels->pDisplayBuffer != NULL)
	{
		pOEP_ExtLevels->pOEP->pDisplay->storeBuffer(pOEP_ExtLevels->pDisplayBuffer);
#ifdef OEP_APP_EXTLEVELS_LOG
		serial_manager.println("[OEP_APP_ExtLevels] Stored display buffer");
#endif
		pOEP_ExtLevels->pOEP->pDisplay->clearDisplay();
		pOEP_ExtLevels->pOEP->pDisplay->display();
	}
	
	// evaluate switches value to preset the right mode
	pOEP_ExtLevels->UpdateDisplay();

	return 0;
}

int OEP_APP_ExtLevels::ButtonReleased(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.println("[OEP_APP_ExtLevels] LongRelease MixIO");
#endif
	
	// save gains and default 
	pOEP_ExtLevels->StoreData();
	
	// restore the screen
	if (pOEP_ExtLevels->pDisplayBuffer != NULL)
	{
		pOEP_ExtLevels->pOEP->pDisplay->restoreBuffer(pOEP_ExtLevels->pDisplayBuffer);
		pOEP_ExtLevels->pOEP->pDisplay->display();
#ifdef OEP_APP_EXTLEVELS_LOG
		serial_manager.println("[OEP_APP_ExtLevels] Restored display buffer");
#endif
		delete[] pOEP_ExtLevels->pDisplayBuffer;
	}
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonPotS1Left(void *pUserData, int iNewPotValue)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);
	
#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.print("[OEP_APP_ExtLevels] PotS1Left = ");
	serial_manager.println(fGain);
#endif
	
	
	pOEP_ExtLevels->UpdateDisplay();

	return 0;
}

int OEP_APP_ExtLevels::ButtonPotS1Right(void *pUserData, int iNewPotValue)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);
	
#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.print("[OEP_APP_ExtLevels] PotS1Right = ");
	serial_manager.println(fGain);
#endif


	pOEP_ExtLevels->UpdateDisplay();

	return 0;
}

int OEP_APP_ExtLevels::ButtonPotS2Left(void *pUserData, int iNewPotValue)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.print("[OEP_APP_ExtLevels] PotS2Left = ");
	serial_manager.println(fGain);
#endif
	
	
	pOEP_ExtLevels->UpdateDisplay();

	return 0;
}

int OEP_APP_ExtLevels::ButtonPotS2Right(void *pUserData, int iNewPotValue)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;
	float fGain = mapfloat(iNewPotValue, 0, 1023, 0.0, 2.0);

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.print("[OEP_APP_ExtLevels] PotS2Right = ");
	serial_manager.println(fGain);
#endif
		

	pOEP_ExtLevels->UpdateDisplay();

	return 0;
}

int OEP_APP_ExtLevels::ButtonSrcSwitchLeft(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateSrcMode(E_SWITCH_STATE_LEFT);
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonSrcSwitchMiddle(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateSrcMode(E_SWITCH_STATE_MIDDLE);
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonSrcSwitchRight(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateSrcMode(E_SWITCH_STATE_RIGHT);
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonVolSwitchLeft(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateVolMode(E_SWITCH_STATE_LEFT);
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonVolSwitchMiddle(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateVolMode(E_SWITCH_STATE_MIDDLE);
	
	return 0;
}

int OEP_APP_ExtLevels::ButtonVolSwitchRight(void *pUserData)
{
	OEP_APP_ExtLevels *pOEP_ExtLevels = (OEP_APP_ExtLevels *)pUserData;

//	pOEP_ExtLevels->UpdateVolMode(E_SWITCH_STATE_RIGHT);
	
	return 0;
}

#if 0
void OEP_APP_ExtLevels::UpdateSrcMode(E_SWITCH_STATE eNewState)
{
	// adapt the mix mode
	switch (eNewState)
	{
		default:
		case E_SWITCH_STATE_LEFT:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Listen to stream 1");
#endif
			eMixMode = E_MIX_MODE_STREAM1;
			break;
		case E_SWITCH_STATE_MIDDLE:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Listen to mix");
#endif
			eMixMode = E_MIX_MODE_MIX;
			break;
		case E_SWITCH_STATE_RIGHT:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Listen to stream 2");
#endif
			eMixMode = E_MIX_MODE_STREAM2;
			break;
	}
	
	// select the mix mode
	select_mix_mode(eMixMode);

	UpdateDisplay();
}

void OEP_APP_ExtLevels::UpdateVolMode(E_SWITCH_STATE eNewState)
{
	// adapt the mix mode
	switch (eNewState)
	{
		case E_SWITCH_STATE_LEFT:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Setting input");
#endif
			eSettingMode = E_SETTING_MODE_INPUT;
			break;
		case E_SWITCH_STATE_MIDDLE:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Setting none");
#endif
			eSettingMode = E_SETTING_MODE_NONE;
			break;
		case E_SWITCH_STATE_RIGHT:
#ifdef OEP_APP_EXTLEVELS_LOG
			serial_manager.println("[OEP_APP_ExtLevels] Setting output");
#endif
			eSettingMode = E_SETTING_MODE_OUTPUT;
			break;
	}
	UpdateDisplay();
}
#endif

void OEP_APP_ExtLevels::UpdateDisplay(void)
{
	switch (pOEP->SwitchLeft_State())
	{
		case E_SWITCH_STATE_LEFT:
		default:
			UpdateDisplay_Levels();
			break;
		case E_SWITCH_STATE_MIDDLE:
			UpdateDisplay_EQ();
			break;
		case E_SWITCH_STATE_RIGHT:
			UpdateDisplay_AVC();
			break;
	}

	pOEP->pDisplay->display();
}

void OEP_APP_ExtLevels::UpdateDisplay_Levels(void)
{
	float inLevelValues[16] = {3.12, 2.63, 2.22, 1.87, 1.58, 1.33, 1.11, 0.94, 0.79, 0.67, 0.56, 0.48, 0.40, 0.34, 0.29, 0.24};
	float outLevelValues[19] = {3.16, 2.98, 2.83, 2.67, 2.53, 2.39, 2.26, 2.14, 2.02, 1.91, 1.80, 1.71, 1.62, 1.53, 1.44, 1.37, 1.29, 1.22, 1.16};
	const int outLevelOffset = 13;
	
	pOEP->pDisplay->setTextSize(1);
	pOEP->pDisplay->setTextColor(WHITE);
	pOEP->pDisplay->setCursor(10,0);
	pOEP->pDisplay->clearDisplay();

	pOEP->pDisplay->print("In   DAC  HP   Out");
	pOEP->pDisplay->print("0.00 100  Off  0.00");
/*	switch (eMixMode)
	{
		default:
		case E_MIX_MODE_STREAM1:
			pOEP->pDisplay->print("In1");
			break;
		case E_MIX_MODE_MIX:
			pOEP->pDisplay->print("Mix");
			break;
		case E_MIX_MODE_STREAM2:
			pOEP->pDisplay->print("In2");
			break;
	}
		pOEP->pDisplay->setCursor(10,10);
		pOEP->pDisplay->print("Out1 : ");
		pOEP->pDisplay->print(fGainS1OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS1OutR, 1);
		pOEP->pDisplay->setCursor(10,20);
		pOEP->pDisplay->print("Out2 : ");
		pOEP->pDisplay->print(fGainS2OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS2OutR, 1);
	*/
}

void OEP_APP_ExtLevels::UpdateDisplay_EQ(void)
{
}

void OEP_APP_ExtLevels::UpdateDisplay_AVC(void)
{
}

#if 0
	pOEP->pDisplay->setTextSize(1);
	pOEP->pDisplay->setTextColor(WHITE);
	pOEP->pDisplay->setCursor(10,0);
	pOEP->pDisplay->clearDisplay();

	pOEP->pDisplay->print("Src: ");
	switch (eMixMode)
	{
		default:
		case E_MIX_MODE_STREAM1:
			pOEP->pDisplay->print("In1");
			break;
		case E_MIX_MODE_MIX:
			pOEP->pDisplay->print("Mix");
			break;
		case E_MIX_MODE_STREAM2:
			pOEP->pDisplay->print("In2");
			break;
	}

	pOEP->pDisplay->print("  Set: ");
	switch (eSettingMode)
	{
		default:
		case E_SETTING_MODE_INPUT:
			pOEP->pDisplay->print("In");
			break;
		case E_SETTING_MODE_NONE:
			pOEP->pDisplay->print("-");
			break;
		case E_SETTING_MODE_OUTPUT:
			pOEP->pDisplay->print("Out");
			break;
	}

	if (eSettingMode == E_SETTING_MODE_INPUT)
	{
		pOEP->pDisplay->setCursor(10,10);
		pOEP->pDisplay->print("In1 : ");
		pOEP->pDisplay->print(fGainS1InL, 2);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS1InR, 2);
		pOEP->pDisplay->setCursor(10,20);
		pOEP->pDisplay->print("In2 : ");
		pOEP->pDisplay->print(fGainS2InL, 2);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS2InR, 2);
	}
	
	if (eSettingMode == E_SETTING_MODE_OUTPUT)
	{
		pOEP->pDisplay->setCursor(10,10);
		pOEP->pDisplay->print("Out1 : ");
		pOEP->pDisplay->print(fGainS1OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS1OutR, 1);
		pOEP->pDisplay->setCursor(10,20);
		pOEP->pDisplay->print("Out2 : ");
		pOEP->pDisplay->print(fGainS2OutL, 1);
		pOEP->pDisplay->print(":");
		pOEP->pDisplay->print(fGainS2OutR, 1);
	}
#endif

#define STORAGE_MAGIC			0x5A02

void OEP_APP_ExtLevels::StoreData(void)
{
	T_STORAGE tData = {0};
	
	// prepare the data
	tData.iValue1 = 1;
	tData.iValue2 = 2;
	tData.iValue3 = 3;
	tData.iValue4 = 4;

	// write the data
	pOEP->Store( STORAGE_MAGIC, iEepromAddress, tData );

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.println("[OEP_APP_ExtLevels] Configuration saved to eeprom");
#endif
}

void OEP_APP_ExtLevels::RestoreData(void)
{
	T_STORAGE tData = {0};
	
	// read the data
	pOEP->Restore( STORAGE_MAGIC, iEepromAddress, tData);
	
	// decode the data
	//fGainS1InL = ((float)tData.iValue1) / 100;
	//fGainS1InR = ((float)tData.iValue2) / 100;
	//fGainS2InL = ((float)tData.iValue3) / 100;
	//fGainS2InR = ((float)tData.iValue4) / 100;

#ifdef OEP_APP_EXTLEVELS_LOG
	serial_manager.println("[OEP_APP_ExtLevels] Configuration reloaded from eeprom");
#endif
}
