/* OpenEffectPedal Applicative helper library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_APP_MixIO__h__
#define __OEP_APP_MixIO__h__


#include "Arduino.h"
#include "OpenEffectPedal.h"	// for the OEP_Abstraction
#include "Audio.h"				// for the mixers

// storage size required by this module:
//  4 bytes for storage header : magic + CRC16
//  8 bytes for the 8 gains stored as an unsigned bytes
//  1 byte for the mode selection
//  7 bytes reserved for future use
#define OEP_APP_MIXIO_STORAGE_SIZE_IN_BYTES				(sizeof(OEP_APP_MixIO::T_STORAGE))

class OEP_APP_MixIO
{
public:

  enum E_MIX_MODE
  {
	  E_MIX_MODE_STREAM1 = 0,
	  E_MIX_MODE_STREAM2,
  	  E_MIX_MODE_MIX,
  };
  
  enum E_UI_MODE
  {
	  E_UI_MODE_OFF = 0,
	  E_UI_MODE_VOL,
	  E_UI_MODE_SRC,
	  E_UI_MODE_VOL_SRC,
  };
  
  enum E_SAVE_MODE
  {
	  E_SAVE_MODE_DISABLED = 0,
	  E_SAVE_MODE_ENABLED,
  };
  
  enum E_LINK_MODE
  {
	  E_LINK_MODE_P1_P2_P3_P4 = 0,	// S1L = P1, S2L = P2, S1R = P3, S2R = P4
	  E_LINK_MODE_P1_P3_P2_P4,		// S1L = P1, S1R = P2, S2L = P3, S2R = P4
	  E_LINK_MODE_P1_P2,			// S1L = S1R = P1, S2L = S2R = P2
	  E_LINK_MODE_P1_P4,			// S1L = S1R = P1, S2L = S2R = P4
  };
  
  struct T_STORAGE
  {
	  uint8_t iGainS1InL;
	  uint8_t iGainS1InR;
	  uint8_t iGainS2InL;
	  uint8_t iGainS2InR;
	  
	  uint8_t iGainS1OutL;
	  uint8_t iGainS1OutR;
	  uint8_t iGainS2OutL;
	  uint8_t iGainS2OutR;

	  uint8_t iMixMode;
	  
	  uint8_t iReserved1;
	  uint8_t iReserved2;
	  uint8_t iReserved3;
	  uint8_t iReserved4;
	  uint8_t iReserved5;
	  uint8_t iReserved6;
	  uint8_t iReserved7;
	  
  };

 

  OEP_APP_MixIO(class OEP_Abstraction *pOEP, class Mixer_Stereo_2to1 *pMix_2to1, class Mixer_Stereo_1to2 *pMix_1to2, E_BUTTON eButton, E_LINK_MODE eLinkMode, E_UI_MODE eUiMode, E_SAVE_MODE eSaveMode, int iEepromAddress) :
	eMixMode(E_MIX_MODE_STREAM1)
  {
	bInitialized = false;
    this->pOEP = pOEP;
    this->pMix_2to1 = pMix_2to1;
    this->pMix_1to2 = pMix_1to2;
	this->eButton = eButton;
	this->eLinkMode = eLinkMode;
	this->eUiMode = eUiMode;
	this->eSaveMode = eSaveMode;
	this->iEepromAddress = iEepromAddress;
	fGainS1InL = 1.0;
	fGainS1InR = 1.0;
	fGainS2InL = 0.0;
	fGainS2InR = 0.0;
	fGainS1OutL = 1.0;
	fGainS1OutR = 1.0;
	fGainS2OutL = 0.0;
	fGainS2OutR = 0.0;
	eSettingMode = E_SETTING_MODE_NONE;
  };
	
  ~OEP_APP_MixIO(void)
  {
	  
  };

  int  setup(void);
  void loop(void);
  
  void select_mix_mode(E_MIX_MODE eMixMode);
  void set_input_gain(float fGainS1Left, float fGainS2Left, float fGainS1Right, float fGainS2Right);
  void set_output_gain(float fGainS1Left, float fGainS2Left, float fGainS1Right, float fGainS2Right);

private:
  enum E_SETTING_MODE
  {
	E_SETTING_MODE_NONE,
	E_SETTING_MODE_INPUT,
	E_SETTING_MODE_OUTPUT,
  };
  
  E_MIX_MODE 		 eMixMode;
  E_SETTING_MODE     eSettingMode;
  E_LINK_MODE        eLinkMode;
  E_UI_MODE 		 eUiMode;
  E_SAVE_MODE 		 eSaveMode;
  E_BUTTON			 eButton;
  E_POT 			 ePotS1L, ePotS1R, ePotS2L, ePotS2R;
  int				 iEepromAddress;

  boolean			 bInitialized;
  
  OEP_Abstraction   *pOEP;
  Mixer_Stereo_2to1 *pMix_2to1;
  Mixer_Stereo_1to2 *pMix_1to2;
  uint8_t			*pDisplayBuffer;

  float				 fGainS1InL;
  float				 fGainS1InR;
  float				 fGainS2InL;
  float				 fGainS2InR;
  float				 fGainS1OutL;
  float				 fGainS1OutR;
  float				 fGainS2OutL;
  float				 fGainS2OutR;
  
  E_MIX_MODE 		 eSavedMode;
  
  int  RegisterCallbacks(void);
  void UnregisterCallbacks(void);
  void UpdateSrcMode(E_SWITCH_STATE eNewState);
  void UpdateVolMode(E_SWITCH_STATE eNewState);
  void UpdateDisplay(void);
  void StoreData(void);
  void RestoreData(void);

  static int ButtonPressed(void *pUserData);
  static int ButtonReleased(void *pUserData);
  static int ButtonPotS1Left(void *pUserData, int iNewPotValue);
  static int ButtonPotS1Right(void *pUserData, int iNewPotValue);
  static int ButtonPotS2Left(void *pUserData, int iNewPotValue);
  static int ButtonPotS2Right(void *pUserData, int iNewPotValue);
  static int ButtonSrcSwitchLeft(void *pUserData);
  static int ButtonSrcSwitchMiddle(void *pUserData);
  static int ButtonSrcSwitchRight(void *pUserData);
  static int ButtonVolSwitchLeft(void *pUserData);
  static int ButtonVolSwitchMiddle(void *pUserData);
  static int ButtonVolSwitchRight(void *pUserData);
};

#endif
