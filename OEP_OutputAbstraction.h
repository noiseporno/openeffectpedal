/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef __OEP_OutputAbstraction__h__
#define __OEP_OutputAbstraction__h__


#include "Arduino.h"
#include "OEP_Defines.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_NeoPixel.h>


class OEP_OutputAbstraction
{
public:

	/*********************************
	*              Types             *
	*********************************/
	#define NUM_LEDS 10
	#define NUM_LEDS_BARGRAPH 7


	/*********************************
	*           Variables            *
	*********************************/
	Adafruit_SSD1306	*pDisplay;
	Adafruit_NeoPixel	*pPixels;
  
  
	/*********************************
	*         Main functions         *
	*********************************/
	OEP_OutputAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType);
	~OEP_OutputAbstraction(void);

	int  setup(void);
	void loop(void);

    
	/*********************************
	*      LED access functions      *
	*********************************/
	// LED bar mode
	void    SetBarGraphMode(boolean Enable);
	boolean GetBarGraphMode(void);
	
	// single LED access
	void    SetLedWithoutShow(E_LED eLed, T_Color tColor);
	void    SetLed(E_LED eLed, T_Color tColor);
	T_Color GetLed(E_LED eLed);

	// use LED strip as a bar graph
	void    SetBarGraphColor(T_Color tColors[NUM_LEDS_BARGRAPH]);
	void	SetBarGraph(uint8_t iLevel);
	void    SetBarGraph(float fLevel);
  
	/*********************************
	*     Screen access function     *
	*********************************/
	void	ClearDisplay(void);
	void 	PrintTextVal(const char *string, int val, T_Color color);

	
private:
	E_MB_Type 	eMotherboardType;
	E_EXT_Type	eExtensionType;

	T_Color		tLed[NUM_LEDS];
	uint8_t		uBargraphLevel;

	T_Color		tBargraphColors[NUM_LEDS_BARGRAPH];
	boolean		bBargraphModeEnable;
	E_LED		aBarGraphMap[NUM_LEDS_BARGRAPH] = {E_LED_1, E_LED_2, E_LED_3, E_LED_4, E_LED_5, E_LED_6, E_LED_7};

	
	boolean		IsBarGraphLed(E_LED eLed);
	
	void 		DisplayLed(E_LED eLed);
	void 		DisplayLeds(void);
	void 		DisplayBarGraph(void);
};


#endif
