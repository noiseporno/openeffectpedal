/* OpenEffectPedal abstraction library
 * Copyright (c) 2017, NoisePorno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "OEP_OutputAbstraction.h"

#include <RBD_SerialManager.h>
extern RBD::SerialManager 			serial_manager;


/*********************************
*             Defines            *
*********************************/
#define PIN         		8	// Data bus pin number for the LED strip
#define NUMPIXELS   		10	// Number of LEDS (NeoPixels) in the strip

#define OLED_RESET  		4	// Pin for the OLED reset

/*********************************
*           Variables            *
*********************************/
static Adafruit_NeoPixel			pixels;
static Adafruit_SSD1306				display(OLED_RESET);
static int 							Pin;
static int							NumPixels;
static int							OLED_Reset;


/*********************************
*         Main functions         *
*********************************/
OEP_OutputAbstraction::OEP_OutputAbstraction(E_MB_Type eMbType, E_EXT_Type eExtType)
{
	eMotherboardType = eMbType;
	eExtensionType = eExtType;
	bBargraphModeEnable = false;
	uBargraphLevel = 0;

	// HW dependent initialization
	Pin = PIN;
	NumPixels = NUMPIXELS;
	OLED_Reset = OLED_RESET;

	// create display
	pPixels = &pixels;
	pDisplay = &display;
}

OEP_OutputAbstraction::~OEP_OutputAbstraction(void)
{
}

int OEP_OutputAbstraction::setup(void)
{
	pixels = Adafruit_NeoPixel(NumPixels, Pin, NEO_GRB + NEO_KHZ800);
	pixels.begin();
  
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x64)
	display.clearDisplay();
	display.display();

	return 0;
}

void OEP_OutputAbstraction::loop(void)
{
}


/*********************************
*      LED access functions      *
*********************************/
// LED bar mode
void OEP_OutputAbstraction::SetBarGraphMode(boolean Enable)
{
	if (Enable == true)
	{
		bBargraphModeEnable = true;
		DisplayBarGraph();
	}
	else
	{
		bBargraphModeEnable = false;
		DisplayLeds();
	}
}

boolean OEP_OutputAbstraction::GetBarGraphMode(void)
{
	return bBargraphModeEnable;
}

boolean  OEP_OutputAbstraction::IsBarGraphLed(E_LED eLed)
{
	if ((eLed >= E_LED_1) && (eLed <= E_LED_7))
		return true;
	else
		return false;
}

// single LED access
void  OEP_OutputAbstraction::SetLedWithoutShow(E_LED eLed, T_Color tColor)
{
	tLed[eLed] = tColor;
}

void OEP_OutputAbstraction::SetLed(E_LED eLed, T_Color tColor)
{
	tLed[eLed] = tColor;
  
	DisplayLeds();
}

T_Color OEP_OutputAbstraction::GetLed(E_LED eLed)
{
	return tLed[eLed];
}

void OEP_OutputAbstraction::DisplayLed(E_LED eLed)
{
	pixels.setPixelColor(eLed, OEP_COLOR_EXTRACT_RED(tLed[eLed]),  OEP_COLOR_EXTRACT_GREEN(tLed[eLed]),  OEP_COLOR_EXTRACT_BLUE(tLed[eLed]));
	pixels.show();
}

void OEP_OutputAbstraction::DisplayLeds(void)
{
	for (uint8_t iLed = (uint8_t)E_LED_UP; iLed <= (uint8_t)E_LED_7; iLed++)
	{
		if ((bBargraphModeEnable == false) ||
			((bBargraphModeEnable == true) && (!IsBarGraphLed((E_LED)iLed))))
		{
			pixels.setPixelColor((E_LED)iLed, OEP_COLOR_EXTRACT_RED(tLed[iLed]),  OEP_COLOR_EXTRACT_GREEN(tLed[iLed]),  OEP_COLOR_EXTRACT_BLUE(tLed[iLed]));
			delay(1);
		}
	}
	pixels.show();
}


// use LED strip as a bar graph
void OEP_OutputAbstraction::SetBarGraphColor(T_Color tColors[NUM_LEDS_BARGRAPH])
{
	for(uint16_t uIndex = 0; uIndex < NUM_LEDS_BARGRAPH; uIndex++)
		tBargraphColors[uIndex] = tColors[uIndex];
}

void OEP_OutputAbstraction::SetBarGraph(uint8_t iLevel)
{
	uint16_t uTemp;

	uTemp = (uint16_t)iLevel * 15;
	uTemp = uTemp >> 9;
	uBargraphLevel = (uint8_t)uTemp;

	if (bBargraphModeEnable == true)
		DisplayBarGraph();
}

void OEP_OutputAbstraction::SetBarGraph(float fLevel)
{
  int iTemp;
  
  iTemp = (int)fLevel * 255;
  if (iTemp < 0)
	  iTemp = 0;
  else if (iTemp > 255)
	  iTemp = 255;
  
  SetBarGraph((uint8_t)iTemp);
}

void OEP_OutputAbstraction::DisplayBarGraph(void)
{
	uint8_t uIndex;

	for (uIndex = 0; uIndex < NUM_LEDS_BARGRAPH; uIndex++)
	{
		if (uIndex < uBargraphLevel)
		{
			pixels.setPixelColor(aBarGraphMap[uIndex], OEP_COLOR_EXTRACT_RED(tBargraphColors[uIndex]),  OEP_COLOR_EXTRACT_GREEN(tBargraphColors[uIndex]),  OEP_COLOR_EXTRACT_BLUE(tBargraphColors[uIndex]));
			
		}
		else
		{
			pixels.setPixelColor(aBarGraphMap[uIndex], 0,  0,  0);
		}
		delay(1);
	}
	pixels.show();
}

/*********************************
*     Screen access function     *
*********************************/
void OEP_OutputAbstraction::ClearDisplay(void)
{
	display.clearDisplay();
	display.display();
}

void OEP_OutputAbstraction::PrintTextVal(const char *string, int val, T_Color color)
{
	display.clearDisplay();
	display.display();
	display.setTextSize(2);
	display.setTextColor(WHITE);
	display.setCursor(10,0);
	display.clearDisplay();
	display.print(string);
	display.print(": ");
	display.print(val);
	display.display();
}
